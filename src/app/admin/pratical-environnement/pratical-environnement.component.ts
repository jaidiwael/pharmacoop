import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { FirestoreService } from '../../services/firestore.service';
import { Observable, Subject, of } from 'rxjs'; 
import {praticalEnvironnement} from "../../classes/pratical-environnement";
@Component({
  selector: 'app-pratical-environnement',
  templateUrl: './pratical-environnement.component.html',
  styleUrls: ['./pratical-environnement.component.scss'],
  providers: [FirestoreService]
})
export class PraticalEnvironnementComponent implements OnInit {

  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<praticalEnvironnement>();
  private listEnvironnement: praticalEnvironnement[];
  private environnementForm: FormGroup;
  private loading = true;
  private titleModal = 'Ajouter un nouveau Environnement';
  private titleButton = 'Ajouter';

  dtOptions: DataTables.Settings = {};

  constructor(private formBuilder: FormBuilder,
    private firestoreService: FirestoreService) {

    this.createForm();
  }

  ngOnInit() {
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getAllEnvironnement();
  }

  rerender(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }

  createForm() {
    this.environnementForm = this.formBuilder.group({
      title: ['', Validators.required],
      uid: ['']
    });
  }

  getAllEnvironnement() {
    this.firestoreService.getAllEnvironnement().subscribe((data) => {
      this.listEnvironnement =  data.reverse();
      this.dtTrigger.next()
      this.loading = false;
    });

  }

  submitEnvironnement(environnementForm) {
    const payload = { title: environnementForm.value.title };
    if (environnementForm.value.uid) {
      this.firestoreService.updateEnvironnement(payload, environnementForm.value.uid) ; 
      this.rerender();    
      this.getAllEnvironnement(); 
    } else {
      this.firestoreService.addEnvironnement(payload)
      this.rerender();    
      this.getAllEnvironnement(); 
    }
    this.resetForm();

  }

  onEdit(environnement) {
    this.titleModal = 'Modifier un environnement';
    this.titleButton = 'Modifier';
    this.environnementForm = this.formBuilder.group({
      title: [environnement.title, Validators.required],
      uid: [environnement.uid]
    });
  }

  onDelete(environnement) {
    if (confirm('Êtes-vous sûr de supprimer cet environnement?') === true) {
      this.firestoreService.removeEnvironnement(environnement.uid)
      this.rerender();    
      this.getAllEnvironnement(); 
    }
  }


  resetForm() {
    this.environnementForm.reset();
    this.titleModal = 'Ajouter un nouveau environnement';
    this.titleButton = 'Ajouter';
  }
}