import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { FirestoreService } from '../../services/firestore.service';
import { Observable, Subject, of } from 'rxjs';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {
  @ViewChild(DataTableDirective) 
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<any>();
  private listExperience: any[]=[];
  private loading = true;
  private experienceForm: FormGroup;
  private titleModal = "Ajouter une nouvelle experience";
  private titleButton = "Ajouter";
  public dtOptions: DataTables.Settings = {};
  constructor(private formBuilder: FormBuilder,
    private firestoreService: FirestoreService) {
    this.createForm();
}
createForm() {
  this.experienceForm = this.formBuilder.group({
    title: ['', Validators.required],
    uid: ['']
  });
}
  ngOnInit() {
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getAllExperience() ; 
  }

  rerender(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }

  getAllExperience() {
    this.firestoreService.getAllExperience().subscribe((data) => {
      this.listExperience = data;
      console.log("listExperience",this.listExperience);
      this.dtTrigger.next();
      this.loading = false;
    });
  }
  submitExperience(experienceForm) {
    const payload = { title: experienceForm.value.title };
    console.log("payload",payload) ;
    if (experienceForm.value.uid) {
      this.firestoreService.updateExperience( experienceForm.value.uid,payload)
      this.getAllExperience() ; 
      this.rerender()
    } else {
      this.firestoreService.addExperience(payload)
      this.getAllExperience() ; 
      this.rerender()
    }
    this.resetForm();

  }

  onEdit(experience) {
    this.titleModal = 'Modifier une experience';
    this.titleButton = 'Modifier';
    this.experienceForm = this.formBuilder.group({
      title: [experience.title, Validators.required],
      uid:   [experience.uid]
    });
  }
  onDelete(experience) {
    console.log("experience",experience) ;
    if (confirm('Êtes-vous sûr de supprimer cet experience?') === true) {
      this.firestoreService.removeExperience(experience.uid)
        this.getAllExperience() ; 
        this.rerender()
      }
  }
  resetForm() {
    this.experienceForm.reset();
    console.log("message");
    this.titleModal = 'Ajouter un nouveau experience';
    this.titleButton = 'Ajouter';
  }


}
