import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Observable, Subject, of } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { JobService } from "../../../services/job.service";
import { Ads } from "../../../classes/ads.class";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-candidates-job',
  templateUrl: './candidates-job.component.html',
  styleUrls: ['./candidates-job.component.scss']
})
export class CandidatesJobComponent implements OnInit {
  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<any>();
  public dtOptions: DataTables.Settings = {};
  private EmployeeParticipat: any[] = [];

  private loading: boolean = true;
  private listUidCandidateJobs: any[] = [];
  private displayDetail: Boolean = false;
  private titleModal: String = '';
  private candidateInfo: any;
  private listEmployeeParticipate: Ads[] = [];
  private uidJob: string;

  constructor(
    public route: ActivatedRoute,
    public jobService: JobService,
    public userService: UserService) {
    this.uidJob = this.route.snapshot.params['uid'];

  }

  ngOnInit() {
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getAllJobs();
  }

  getAllJobs() {
    this.jobService.getAdsByUid(this.uidJob).subscribe((ad: any) => {
      let i = 0;
      if (ad.listUidEmployeeParticipate.length) {
        for (let uidEmployee of ad.listUidEmployeeParticipate) {
          this.userService.getEmployeeInfoByUid(uidEmployee).subscribe((employee: any) => {
            this.listEmployeeParticipate.push(employee);
            i++;
            if (i === ad.listUidEmployeeParticipate.length) this.dtTrigger.next();
          })
        }
      }
      this.loading = false;
    })
  }

  rerender(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.unsubscribe();
    });
  }

  onDisplay(candidate: any) {
    this.displayDetail = true;
    this.titleModal = 'Informations candidate';
    this.candidateInfo = candidate;
    this.dtTrigger.unsubscribe();
  }

}
