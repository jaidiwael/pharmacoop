import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shared-register',
  templateUrl: './shared-register.component.html',
  styleUrls: ['./shared-register.component.scss']
})
export class SharedRegisterComponent implements OnInit {

  userForm: FormGroup;
  loading = false;
  @Input() public owner: string;

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder) {

    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.userForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    });
  }

  goLoginPage() {
    this.router.navigate(['/' + this.owner + '/login']);
  }

  submit(userForm) {
    console.log(userForm.value);
    this.loading = true;
    const payload = this.getInfoUser(this.owner, userForm.value.email, userForm.value.password);
    this.authService.signUpUser(payload, this.owner)
      .then((data: any) => {
        if (data) {
          (!data.message) ? this.router.navigate(['/' + this.owner + '/login']) : alert(data.message);
          this.loading = false;
        }
      });

  }

  signUpWithFaceBook() {
    this.loading = true;
    this.authService.signUpWithGoogle(this.owner).then((data) => {
      console.log(data);
      if (data) {
        (!data.message) ? this.router.navigate(['/' + this.owner + '/login']) : alert(data.message);
        this.loading = false;
      }
    });


  }

  signUpWithGoogle() {
    this.loading = true;
    this.authService.signUpWithGoogle(this.owner).then((data) => {
      console.log(data);
      if (data) {
        (!data.message) ? this.router.navigate(['/' + this.owner + '/login']) : alert(data.message);
        this.loading = false;
      }
    });
  }

  signUpWithTwitter() {
    this.loading = true;
    this.authService.signUpWithGoogle(this.owner).then((data) => {
      console.log(data);
      if (data) {
        (!data.message) ? this.router.navigate(['/' + this.owner + '/login']) : alert(data.message);
        this.loading = false;
      }
    });
  }

  getInfoUser(owner, email, password) {
    let payload = {};
    switch (owner) {
      case 'employee':
        payload = {
          email: email,
          password: password,
          verifyProfile: false,
          blockProfile: false,
          EntrepriseName: '',
          firstName: '',
          lastName: '',
          phoneNumber: '',
          verifyNumberPhone: false,
          taxesTPSTVQ: false,
          RAMQ: '',
          professionalOrderNumber: '',
          category: {},
          listEnvironnement: [],
          listSoftware: [],
          listStudyLevel: [],
          yearsExperience: '',
          cvUrl: '',
          cvFileName: '',
          cinUrl: '',
          cinFileName: '',
          licenseUrl: '',
          licenseFileName: '',
          authorizedSmsEmail: false,
          validDocuments: false,
          descriptionProfile: ''
        };
        break;
      case 'employer':
        payload = {
          email: email,
          password: password,
          verifyProfile: false,
          blockProfile: false,
          gender: 'Madame',
          firstName: '',
          lastName: '',
          positionHeld: '',
          phoneNumber: '',
          verifyNumberPhone: false,
          authorizedSmsEmail: false,
          validDocuments: false,
        };
        break;
      case 'admin':
        payload = {
          email: email,
          password: password,
        };
        break;
    }
    return payload; 
  }

}
