import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedDisplayEmployeeComponent } from './shared-display-employee.component';

describe('SharedDisplayEmployeeComponent', () => {
  let component: SharedDisplayEmployeeComponent;
  let fixture: ComponentFixture<SharedDisplayEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedDisplayEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedDisplayEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
