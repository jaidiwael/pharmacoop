import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from "../../services/user.service";
import { User } from "../../classes/user.class";
import { Observable, Subject, of } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
@Component({
  selector: 'app-invalid-employee',
  templateUrl: './invalid-employee.component.html',
  styleUrls: ['./invalid-employee.component.scss']
})
export class InvalidEmployeeComponent implements OnInit {
  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<any>();
  private dtOptions: DataTables.Settings = {};
  private loading = false;
  private displayDetail: boolean = false;
  private titleModal: String = 'Detail employer';
  private titleButton: String = 'Ajouter';
  private employeeInfo: any;
  private listEmployee: any[];
  constructor(private userService: UserService) { }

  ngOnInit() {
    console.log("message");
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getAllListInvalidEmployee();
  }


  getAllListInvalidEmployee() {
    this.userService.getAllInvalidEmployee().subscribe((data: any) => {
      this.listEmployee = data;
      this.dtTrigger.next();
      this.loading = false;
    }, error => {
      console.log("error", error);
    })
  }
  rerender(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }


  onDisplay(employee: any) {
    this.displayDetail = true;
    console.log("employee", employee);
    // this.titleModal = 'Informations Employers';
    this.employeeInfo = employee;

  }

  changeStatusEmployee(uid: string) {
    if (confirm('Êtes-vous sûr de valider ce emplyee') === true) {
      console.log(uid);
      let user: object = {
        verifyProfile: true
      }
      this.userService.changeStatusEmployee(user, uid);
      this.getAllListInvalidEmployee();
    }
  }
}

