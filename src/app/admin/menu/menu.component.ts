import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(private navbarService: NavbarService) { }

  ngOnInit() {
  }

  logout() {
    this.navbarService.logout("admin/login");
  }

}
