import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ExperienceComponent} from "./experience/experience.component" ; 
import {FormEmploymentComponent} from "./form-employment/form-employment.component" ; 
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { CategoriesComponent } from './categories/categories.component';
import { SoftwareComponent } from './software/software.component';
import { PraticalEnvironnementComponent } from './pratical-environnement/pratical-environnement.component';
import { StudyLevelsComponent } from './study-levels/study-levels.component';
import {EmployeeComponent} from "./employee/employee.component" ;
import { InvalidEmployeeComponent } from './invalid-employee/invalid-employee.component';
import {EmployerComponent} from "./employer/employer.component" ;
import { InvalidEmployerComponent } from './invalid-employer/invalid-employer.component';
import { MandatesComponent } from './remplacement/mandates/mandates.component';
import { ConfirmedMandatesComponent } from './remplacement/confirmed-mandates/confirmed-mandates.component';
import { ClosedMandatesComponent } from './remplacement/closed-mandates/closed-mandates.component';
import { CandidatesMandatesComponent } from './remplacement/candidates-mandates/candidates-mandates.component';
import { AdsJobComponent } from './job/ads-job/ads-job.component';
import { CandidatesJobComponent } from './job/candidates-job/candidates-job.component';
const routes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    {   
        path: '', component: HomeComponent,
        children: [
            { path: 'employee', component: EmployeeComponent },
            { path: 'invalid-employee', component: InvalidEmployeeComponent },
            { path: 'employer', component: EmployerComponent },
            { path: 'invalid-employer', component: InvalidEmployerComponent },
            { path: 'categories', component: CategoriesComponent },
            { path: 'software', component: SoftwareComponent },
            { path: 'pratical-environnement', component: PraticalEnvironnementComponent },
            { path: 'study-levels', component: StudyLevelsComponent },
            { path: 'experience', component: ExperienceComponent },
            { path: 'FormEmployment', component: FormEmploymentComponent },
            { path: 'mandates', component: MandatesComponent },
            { path: 'candidates-mandates/:uid', component: CandidatesMandatesComponent },
            { path: 'confirmed-mandates', component: ConfirmedMandatesComponent },
            { path: 'colsed-mandates', component: ClosedMandatesComponent },
            { path: 'ads-job', component: AdsJobComponent },
            { path: 'candidates-job/:uid', component: CandidatesJobComponent },
        ]
    },
    { path: 'login', component: LoginComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
