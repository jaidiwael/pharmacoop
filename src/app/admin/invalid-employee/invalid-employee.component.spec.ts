import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvalidEmployeeComponent } from './invalid-employee.component';

describe('InvalidEmployeeComponent', () => {
  let component: InvalidEmployeeComponent;
  let fixture: ComponentFixture<InvalidEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvalidEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvalidEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
