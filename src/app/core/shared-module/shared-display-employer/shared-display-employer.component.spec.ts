import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedDisplayEmployerComponent } from './shared-display-employer.component';

describe('SharedDisplayEmployerComponent', () => {
  let component: SharedDisplayEmployerComponent;
  let fixture: ComponentFixture<SharedDisplayEmployerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedDisplayEmployerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedDisplayEmployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
