import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


//classes
import { Category } from "../classes/category.class";
import { praticalEnvironnement } from "../classes/pratical-environnement";
import { StudyLevel } from "../classes/study-levels.class";
import { Software } from "../classes/software.class"

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  private categoriesCollection: any;
  private softwareCollection: any;
  private environnementCollection: any;
  private studyLevelsCollection: any;
  private formEmploymentCollection: any;
  private experienceCollection: any;
  constructor(private afs: AngularFirestore) {
    this.categoriesCollection = this.afs.collection('categories');
    this.softwareCollection = this.afs.collection('software');
    this.environnementCollection = this.afs.collection('environnement');
    this.studyLevelsCollection = this.afs.collection('studyLevels');
    this.formEmploymentCollection = this.afs.collection('formEmployment');
    this.experienceCollection = this.afs.collection('experience');
  }

  addCategory(category: Category) {
    return this.categoriesCollection.add(category);
  }

  removeCategory(uidCategory: String) {
    return this.afs.doc(`categories/${uidCategory}`).delete();
  }

  updateCategory(category: Category, uidCategory: String) {
    return this.afs.doc(`categories/${uidCategory}`).update(category);
  }

  getAllCategroy() {
    return this.categoriesCollection.snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const uid = a.payload.doc.id;
        return { uid, ...data };
      }))
    );
  }

  addSoftware(Software: Software) {
    return this.softwareCollection.add(Software);
  }

  removeSoftware(uidSoftware: String) {
    return this.afs.doc(`software/${uidSoftware}`).delete();
  }

  updateSoftware(software: Software, uidSoftware: String) {
    return this.afs.doc(`software/${uidSoftware}`).update(software);
  }

  getAllSoftware() {
    return this.softwareCollection.snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const uid = a.payload.doc.id;
        return { uid, ...data };
      }))
    );
  }




  addEnvironnement(environnement: praticalEnvironnement) {
    return this.environnementCollection.add(environnement);
  }
  

  removeEnvironnement(uidEnvironnement: String) {
    return this.afs.doc(`environnement/${uidEnvironnement}`).delete();
  }

  updateEnvironnement(environnement: praticalEnvironnement, uidEnvironnement: String) {
    return this.afs.doc(`environnement/${uidEnvironnement}`).update(environnement);
  }

  getAllEnvironnement() {
    return this.environnementCollection.snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const uid = a.payload.doc.id;
        return { uid, ...data };
      }))
    );
  }

  addStudyLevels(studyLevels: StudyLevel) {
    return this.studyLevelsCollection.add(studyLevels);
  }

  removeStudyLevels(uidStudyLevels: String) {
    return this.afs.doc(`studyLevels/${uidStudyLevels}`).delete();
  }

  updateStudyLevels(studyLevels: StudyLevel, uidStudyLevels: String) {
    return this.afs.doc(`studyLevels/${uidStudyLevels}`).update(studyLevels);
  }

  getAllStudyLevels() {
    return this.studyLevelsCollection.snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const uid = a.payload.doc.id;
        return { uid, ...data };
      }))
    );
  }



  removeExperience(uidExperience: string) {
    console.log("UidExperience",uidExperience);
    return this.afs.doc(`experience/${uidExperience}`).delete();
  }
  updateExperience(uidExperience: string, experience: any) {
    console.log("UidExperience",uidExperience,"experience",experience);
    return this.afs.doc(`experience/${uidExperience}`).update(experience);
  }
  addExperience(experience: any) {
    console.log("experience",experience) ;
    return this.experienceCollection.add(experience);
  }
  getAllExperience() {
    return this.experienceCollection.snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const uid = a.payload.doc.id;
        return { uid, ...data };
      }))
    );
  }



  removeFormEmployment(uidFormEmployment: string) {
    return this.afs.doc(`formEmployment/${uidFormEmployment}`).delete();
  }
  updateFormEmployment(uidFormEmployment: string, FormEmployment: any) {
    return this.afs.doc(`formEmployment/${uidFormEmployment}`).update(FormEmployment);
  }
  addFormEmployment(FormEmployment: any) {
    return this.formEmploymentCollection.add(FormEmployment);
  }
  getAllFormEmployment() {
    return this.formEmploymentCollection.snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const uid = a.payload.doc.id;
        return { uid, ...data };
      }))
    );
  }



}
