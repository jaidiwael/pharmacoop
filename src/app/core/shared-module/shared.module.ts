import { NgModule } from '@angular/core';

import { LoadingModule } from 'ngx-loading';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';


import { SharedMandateComponent } from './shared-mandate/shared-mandate.component';
import { SharedLoginComponent } from './shared-login/shared-login.component';
import { SharedRegisterComponent } from './shared-register/shared-register.component';
import { SharedUserComponent } from './shared-user/shared-user.component';
import { SharedAdsComponent } from './shared-ads/shared-ads.component';
import { SharedDisplayEmployerComponent } from './shared-display-employer/shared-display-employer.component';
import { SharedDisplayEmployeeComponent } from './shared-display-employee/shared-display-employee.component';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        LoadingModule,
        DataTablesModule,
        RouterModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule
    ],
    declarations: [SharedDisplayEmployerComponent, SharedDisplayEmployeeComponent, SharedMandateComponent, SharedAdsComponent, SharedUserComponent, SharedRegisterComponent, SharedLoginComponent],
    exports: [SharedDisplayEmployerComponent, SharedDisplayEmployeeComponent,  SharedMandateComponent, SharedAdsComponent, SharedUserComponent, SharedRegisterComponent, SharedLoginComponent]

})
export class SharedModule { }