import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from "../classes/user.class";

@Injectable({
  providedIn: 'root'
})

export class UserService {
  private adminsCollection: any;
  private employersCollection: any;
  private employeesCollection: any;

  constructor(private afs: AngularFirestore,
    private afAuth: AngularFireAuth) {
    this.adminsCollection = this.afs.collection('admins');
    this.employersCollection = this.afs.collection('employers');
    this.employeesCollection = this.afs.collection('employees');
  }

  saveUserInfo(uid: String, user, profession) {
    let collectionProfession: any;
    let payload = {};
    switch (profession) {
      case 'employer':
        collectionProfession = this.employersCollection;
        payload = user;
        break;
      case 'employee':
        collectionProfession = this.employeesCollection;
        payload = user;
        break;
      case 'admin':
        collectionProfession = this.adminsCollection;
        payload = { email: user.email };
        break;
    }
    return collectionProfession.doc(uid).set(payload);
  }

  getUidCurrentUser() {
    return this.afAuth.auth.currentUser.uid;
  }

  getUserInfoByUid(owner, EmployeeUid: string) {
    switch (owner) {
      case 'employee':
        return this.employeesCollection.doc(EmployeeUid).valueChanges();
      case 'employer':
        return this.employersCollection.doc(EmployeeUid).valueChanges();
    }
  }

  /** Employer */

  getInfoCurrenEmployers() {
    return this.getCurrentUser().then((user) => {
      return this.employersCollection.doc(user.uid).valueChanges();
    });
  }
  getEmployerInfo() {
    return this.employersCollection.doc(this.getUidCurrentUser()).valueChanges();
  }

  getEmployerInfoByUid(EmployerUid: String) {
    return this.employersCollection.doc(EmployerUid).valueChanges();
  }

  getListEmployers() {
    return this.employersCollection.snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }
  removeEmployer(uid: String) {
    return this.employersCollection.doc(uid).delete();
  }
  updateEmployer(user: User) {
    return this.employersCollection.doc(this.getUidCurrentUser()).update(user);
  }
  /** Employee */

  getCurrentUser() {
    return new Promise<any>((resolve, reject) => {
      var user = this.afAuth.auth.onAuthStateChanged(function (user) {
        if (user) {
          resolve(user);
        } else {
          reject('No user logged in');
        }
      })
    })
  }

  getInfoCurrenEmployees() {
    return this.getCurrentUser().then((user) => {
      return this.employeesCollection.doc(user.uid).valueChanges();
    });
  }

  getEmployeeInfoByUid(EmployeeUid: string) {
    return this.employeesCollection.doc(EmployeeUid).valueChanges();
  }
  getEmployeeInfo() {
    return this.employeesCollection.doc(this.getUidCurrentUser()).valueChanges();
  }
  getListEmployees() {
    return this.employeesCollection.snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }
  updateEmployee(user: User) {
    return this.employeesCollection.doc(this.getUidCurrentUser()).update(user);
  }
  removeEmployee(uid: String) {
    return this.employeesCollection.doc(uid).delete();
  }

  //admin
  getAdminInfoByUid(AdminUid: String) {
    console.log("userUid", AdminUid);
    return this.adminsCollection.doc(AdminUid).valueChanges();
  }

  getAllInvalidEmployee() {
    return this.afs.collection("employees", ref => ref.where('verifyProfile', '==', false)).snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }
  changeStatusEmployee(user, uid: string) {
    return this.employeesCollection.doc(uid).update(user);
  }
  getAllInvalidEmployer() {
    return this.afs.collection("employers", ref => ref.where('validate', '==', false)).snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }
  changeStatusEmployer(user, uid: string) {
    return this.employersCollection.doc(uid).update(user);
  }
  getAllValidEmployee() {
    return this.afs.collection("employees", ref => ref.where('verifyProfile', '==', true)).snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }


  getAllValidEmployer() {
    return this.afs.collection("employers", ref => ref.where('validate', '==', true)).snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }



}
