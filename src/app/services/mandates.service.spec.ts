import { TestBed, inject } from '@angular/core/testing';

import { MandatesService } from './mandates.service';

describe('MandatesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MandatesService]
    });
  });

  it('should be created', inject([MandatesService], (service: MandatesService) => {
    expect(service).toBeTruthy();
  }));
});
