export class Mandates {
    uid?: string;
    refMandates?: string;
    uidEmployer?: string;
    uidStore?: string;
    store?: any;
    uidCategory?: string;
    category?: any;
    rangeDate?: Date[];
    startDate?: Date;
    endDate?: Date;
    createdDate?: Date;
    lastEditDate?: Date;
    cancelDate?: Date;
    pricePerHour?: string;
    nbHour?: Number;
    total?: Number;
    listUidApplicatedEmployee?: any[];
    uidSelectedEmployee?: string;
    listSoftware?: any[];
    listUidEmployeeParticipate?: any[];
    MandateStatus?: string;
    publish?: boolean;
}
