export class Ads {
        uid?: String;
        uidEmployer?:String
        uidCategory?: String;
        uidStore?: String; 
        category?: any;
        title?: String;
        post?: String ; 
        createdDate?: any;
        detail?: String ; 
        startDate?:any ;
        store?: any;
        yearsExprence?:String ;
        softwareList?:any[] ; 
        refAnnonce?:string ;
        listUidEmployeeParticipate?:any[] ; 
}