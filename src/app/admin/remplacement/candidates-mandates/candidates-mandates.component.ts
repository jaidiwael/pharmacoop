import { Component, OnInit, ViewChild } from '@angular/core';
import { MandatesService } from "../../../services/mandates.service"
import { Mandates } from "../../../classes/mandates.class";
import { UserService } from '../../../services/user.service';
import { Observable, Subject, of } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-candidates-mandates',
  templateUrl: './candidates-mandates.component.html',
  styleUrls: ['./candidates-mandates.component.scss']
})
export class CandidatesMandatesComponent implements OnInit {


  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<any>();
  public dtOptions: DataTables.Settings = {};

  private loading: boolean = true;
  private mandat: any;
  private candidateInfo: any;
  private listMandates: any[] = [];
  private listEmployeeParticipate: any[] = []
  private mandate: Mandates = new Mandates()
  private uidCurrentUser: string;
  private displayDetail: Boolean = false;
  private titleModal: String = '';
  private uidMandate: string;

  constructor(
    public route: ActivatedRoute,
    public mandatesService: MandatesService,
    public userService: UserService) {

    this.uidMandate = this.route.snapshot.params['uid'];
    this.uidCurrentUser = this.userService.getUidCurrentUser();

  }
  ngOnInit() {
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getAllMandates();
  }

  getAllMandates() {
    console.log("message");
    this.mandatesService.getMandatesByUid(this.uidMandate).subscribe((mandate: any) => {
      console.log("mandate", mandate);
      let i = 0;
      if (mandate.listUidEmployeeParticipate.length) {
        for (let uidEmployee of mandate.listUidEmployeeParticipate) {
          this.userService.getEmployeeInfoByUid(uidEmployee).subscribe((employee: any) => {
            this.listEmployeeParticipate.push(employee);
            console.log("this.listEmployeeParticipate", this.listEmployeeParticipate);
            i++;
            if (i === mandate.listUidEmployeeParticipate.length) this.dtTrigger.next();
          })
        }
      }
      this.loading = false;



    }, error => {
      console.log("error", error);
    })

  }



  rerender(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }
  onDisplay(candidate: any) {
    console.log("candidate", candidate);
    this.displayDetail = true;
    this.titleModal = 'Informations candidate';
    this.candidateInfo = candidate;
  }
}







































































