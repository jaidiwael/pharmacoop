import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedRegisterComponent } from './shared-register.component';

describe('SharedRegisterComponent', () => {
  let component: SharedRegisterComponent;
  let fixture: ComponentFixture<SharedRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
