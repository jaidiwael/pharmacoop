import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { FirestoreService } from '../../services/firestore.service';
import { Observable, Subject, of } from 'rxjs';
import { Category } from "../../classes/category.class";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  providers: [FirestoreService]
})
export class CategoriesComponent implements OnInit {
  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<Category>();
  private listCategory: Category[];
  private loading = true;
  private category : Category = {}
  private categoryForm: FormGroup;
  private titleModal = 'Ajouter une nouvelle category';
  private titleButton = 'Ajouter';
  public dtOptions: DataTables.Settings = {};
  constructor(private formBuilder: FormBuilder,
    private firestoreService: FirestoreService) {
    this.createForm();
  }
  createForm() {
    this.categoryForm = this.formBuilder.group({
      title: ['', Validators.required],
      uid: [''] , 
      orderNumber : ['']
    });
  }

  ngOnInit() {
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getAllCategory();
  }


  getAllCategory() {
        this.listCategory = [];
        this.firestoreService.getAllCategroy().subscribe((data: any[]) => {
        this.listCategory = data.reverse();
        this.dtTrigger.next();
        this.loading = false;
    });

  }

  rerender(): void {
      this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }
  submitCategory(categoryForm) {
    const payload = { title: categoryForm.value.title , orderNumber : categoryForm.value.orderNumber };
    if (categoryForm.value.uid) {
      this.firestoreService.updateCategory(payload, categoryForm.value.uid);
      this.getAllCategory();
      this.rerender();
    } else {
      this.firestoreService.addCategory(payload);
      this.getAllCategory();
      this.rerender();
    }
    this.resetForm();

  }

  onEdit(category) {
    this.titleModal = 'Modifier une category';
    this.titleButton = 'Modifier';
      this.categoryForm = this.formBuilder.group({
      title: [category.title, Validators.required],
      uid: [category.uid],
      orderNumber : [category.orderNumber]
    });
  }

  onDelete(category) {
    if (confirm('Êtes-vous sûr de supprimer cet category?') === true) {
      this.firestoreService.removeCategory(category.uid);
      this.getAllCategory();
      this.rerender();
    }
  }
  resetForm() {
    this.categoryForm.reset();
    this.titleModal = 'Ajouter un nouvellle category';
    this.titleButton = 'Ajouter';
  }
}
