import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PraticalEnvironnementComponent } from './pratical-environnement.component';

describe('PraticalEnvironnementComponent', () => {
  let component: PraticalEnvironnementComponent;
  let fixture: ComponentFixture<PraticalEnvironnementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PraticalEnvironnementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PraticalEnvironnementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
