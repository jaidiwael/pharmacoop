import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-closed-mandates',
  templateUrl: './closed-mandates.component.html',
  styleUrls: ['./closed-mandates.component.scss']
})
export class ClosedMandatesComponent implements OnInit {
  public payload : object =  {
    statusMandate : 'closed' , 
    owner : 'employer',
    method:'getListClosedMandateByEmployer',
    parentLink:'employer/mandates',
    titlePage: 'Mes mandats finalisés',
  }
  
  constructor(){}
  ngOnInit() {}
   
  

 

}
