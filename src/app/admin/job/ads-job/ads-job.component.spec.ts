import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdsJobComponent } from './ads-job.component';

describe('AdsJobComponent', () => {
  let component: AdsJobComponent;
  let fixture: ComponentFixture<AdsJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdsJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdsJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
