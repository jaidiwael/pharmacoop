import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-mandates',
  templateUrl: './mandates.component.html',
  styleUrls: ['./mandates.component.scss']
})
export class MandatesComponent implements OnInit {

  public payload: object = {
    statusMandate: 'free',
    owner: 'employee',
    method:'applyToThisMandate',
    parentLink:'employee/mandates',
    titlePage:'Liste des mandats',
  }
  constructor() { }
  ngOnInit() { }
}
