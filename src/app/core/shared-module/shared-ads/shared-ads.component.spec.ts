import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedAdsComponent } from './shared-ads.component';

describe('SharedAdsComponent', () => {
  let component: SharedAdsComponent;
  let fixture: ComponentFixture<SharedAdsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedAdsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedAdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
