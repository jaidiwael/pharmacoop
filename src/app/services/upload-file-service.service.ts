import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';
import { FileUpload } from '../classes/fileupload.class';
import { Employee } from "../classes/employee.class";

@Injectable({
  providedIn: 'root'
})
export class UploadFileServiceService {

  private employeesCollection: any;

  constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth) {
    this.employeesCollection = this.afs.collection('employees');
  }

  getUidCurrentUser() {
    return this.afAuth.auth.currentUser.uid;
  }

  deleteFileUpload(name: string, path: string, uid: String) {
    let user: Employee;
    const storageRef = firebase.storage().ref();
    storageRef.child(`${path}/${name}`).delete();
    switch (path) {
      case "/CIN":
        user = { cinUrl: "", cinFileName: "" }
        break;
      case "/CVS":
        user = { cvUrl: "", cvFileName: "" }
        break;
      case "/licenses":
        user = { cinFileName: "", licenseFileName: "" }
        break;
    }
    return this.employeesCollection.doc(uid).update(user);
  }
  pushFileToStorage(fileUpload: FileUpload, uid: String, path: String) {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${path}/${fileUpload.file.name}`).put(fileUpload.file);
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        const snap = snapshot as firebase.storage.UploadTaskSnapshot;
      },
      (error) => {
        console.log(error);
      },
      () => {
        uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
          fileUpload.url = downloadURL;
          fileUpload.name = fileUpload.file.name;
          this.saveFileData(fileUpload, uid, path).then((data) => {
            console.log('saveFileData', data);
          })
        });

      }
    );
  }
  private saveFileData(fileUpload: FileUpload, uid: String, path: String) {
    let user: Employee;
    switch (path) {
      case "/CIN":
        user = { cinUrl: fileUpload.url, cinFileName: fileUpload.name }
        break;
      case "/CVS":
        user = { cvUrl: fileUpload.url, cvFileName: fileUpload.name }
        break;
      case "/licenses":
        user = { licenseUrl: fileUpload.url, licenseFileName: fileUpload.name }
        break;
    }
    return this.employeesCollection.doc(uid).update(user);
  }









}
