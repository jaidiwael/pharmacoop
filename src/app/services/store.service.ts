import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Store} from "../classes/store.class"  ; 



@Injectable({
  providedIn: 'root'
})
export class StoreService {
  private employersCollection: any;

  constructor(private afs: AngularFirestore) {
    this.employersCollection = this.afs.collection('employers');
  }

  addStore(uid:String, store:Store) {
      return this.employersCollection.doc(uid).collection('stores').add(store);
  }

  removeStore(uid : String, uidstore : String) {
        return this.employersCollection.doc(uid).collection('stores').doc(uidstore).delete();
  }

  updateStore(uid: String, uidstore: String, store :Store) {
        return this.employersCollection.doc(uid).collection('stores').doc(uidstore).update(store);
  }

  getAllStore(uidUser: String) {
        return this.employersCollection.doc(uidUser).collection('stores')
          .snapshotChanges().pipe(
          map((actions: any) => actions.map(a => {
            const data = a.payload.doc.data();
            const uid = a.payload.doc.id;
            return { uid, ...data };
          }))
          );
  }

}
