import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from "../../services/user.service"

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AuthService]
})
export class LoginComponent implements OnInit {

  userForm: FormGroup;
  loading = false;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.userForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    });
  }

  submit(userForm) {
  /*  this.loading = true;
    const payload = {
      email: userForm.value.email,
      password: userForm.value.password
    }
    this.authService.signUpUser(payload, "admin")*/
   this.authService.signInUser(userForm.value.email, userForm.value.password)
      .then((data: any) => {
        if (!data.message) {
          this.userService.getAdminInfoByUid(data.user.uid).subscribe(user => {
            if (user) {
              this.router.navigate(['/admin/employee'])
            } else {
              alert("Accès interdit");
              this.userForm.reset();
            }
          })
        } else {
          alert(data.message);
          this.userForm.reset();
        }
        this.loading = false;
      });

  }

}
