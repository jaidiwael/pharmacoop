import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { EmployeeRoutingModule } from './employee.routing';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { MenuComponent } from './menu/menu.component';
import { MandatesComponent } from './remplacement/mandates/mandates.component';
import { PendingMandatesComponent } from './remplacement/pending-mandates/pending-mandates.component';
import { ConfirmedMandatesComponent } from './remplacement/confirmed-mandates/confirmed-mandates.component';
import { ClosedMandatesComponent } from './remplacement/closed-mandates/closed-mandates.component';
import { SearchJobComponent } from './job/search-job/search-job.component';
import { AppliedJobComponent } from './job/applied-job/applied-job.component';
import { LoadingModule } from 'ngx-loading';
import { DataTablesModule } from 'angular-datatables';
import { ToastrModule } from 'ngx-toastr';
import { NgxMaskModule } from 'ngx-mask';
import { SharedModule } from "../core/shared-module/shared.module";


@NgModule({
    declarations: [
        LoginComponent,
        HomeComponent,
        RegisterComponent,
        ProfileComponent,
        MenuComponent,
        MandatesComponent,
        PendingMandatesComponent,
        ConfirmedMandatesComponent,
        ClosedMandatesComponent,
        SearchJobComponent,
        AppliedJobComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        EmployeeRoutingModule,
        HttpModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        LoadingModule,
        DataTablesModule,
        ToastrModule.forRoot({ timeOut: 3000 }),
        NgxMaskModule.forRoot(),
        SharedModule
    ],
})
export class EmployeeModule { }
