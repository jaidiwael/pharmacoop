import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEmploymentComponent } from './form-employment.component';

describe('FormEmploymentComponent', () => {
  let component: FormEmploymentComponent;
  let fixture: ComponentFixture<FormEmploymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormEmploymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEmploymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
