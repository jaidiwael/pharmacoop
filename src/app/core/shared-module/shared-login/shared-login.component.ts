import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-shared-login',
  templateUrl: './shared-login.component.html',
  styleUrls: ['./shared-login.component.scss']
})
export class SharedLoginComponent implements OnInit {

  @Input() public owner: string;

  userForm: FormGroup;
  loading = false;
  hidden: boolean = false;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.userForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    });
  }

  goRegisterPage() {
    this.router.navigate(['/' + this.owner + '/register']);
  }

  submit(userForm) {
    this.loading = true;
    this.authService.signInUser(userForm.value.email, userForm.value.password)
      .then((data: any) => {
        console.log("employee", data);
        if (!data.message) {
          this.userService.getUserInfoByUid(this.owner, data.user.uid).subscribe(user => {
            if (user) {
              this.router.navigate(['/' + this.owner + '/profile'])
            } else {
              alert("Accès interdit");
              this.userForm.reset();
            }
          })
        } else {
          alert(data.message);
          this.userForm.reset();
        }
        this.loading = false;
      });
  }

  signInWithGoogle() {
    console.log("signInWithGoogle");
    this.loading = true;
    this.authService.signInWithGoogle().then((data) => {
      console.log(data);

      if (data) {
        (!data.message) ? this.router.navigate(['/employee/profile']) : alert(data.message);
        this.loading = false;
      }
    });
  }
  /*
   signInWithTwitter() {
     this.loading = true;
     this.authService.signInWithTwitter().then((data) => {
       if (data) {
         (!data.message) ? this.router.navigate(['/home/offer']) : alert(data.message);
         this.loading = false;
       }
     });
   }
  */

}
