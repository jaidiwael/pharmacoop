import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { UserService } from './user.service';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public authState: any = null;
  constructor(private afAuth: AngularFireAuth,
    private userService: UserService) {}
  



  signUpWithGoogle(profession) {
    const providerGoogle = new firebase.auth.GoogleAuthProvider();
    return this.afAuth.auth.signInWithPopup(providerGoogle)
      .then((result) => {
        this.userService.saveUserInfo(result.user.uid, result.user.email, profession);
        return true;
      }).catch(error => error);
  }

  signUpWithFacebook(profession) {
    const providerFacebook = new firebase.auth.FacebookAuthProvider();
    return this.afAuth.auth.signInWithPopup(providerFacebook)
      .then((result) => {
        this.userService.saveUserInfo(result.user.uid, result.user.email, profession);
        return true;
      }).catch(error => error);
  }

  signUpWithTwitter(profession) {
    const providerFacebook = new firebase.auth.TwitterAuthProvider();
    return this.afAuth.auth.signInWithPopup(providerFacebook)
      .then((result) => {
        this.userService.saveUserInfo(result.user.uid, result.user.email, profession);
        return true;
      }).catch(error => error);
  }

  signUpUser(user, profession) {
    return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
      .then((result) => {
       // this.sendEmailVerification()
        user.uid = result.user.uid
        this.userService.saveUserInfo(result.user.uid, user, profession);
        return true;
      }).catch(error => error);
  }

  signInUser(email: string, password: string) {
    return firebase.auth().signInWithEmailAndPassword(email, password)
      .then((data) => {
        return data;
      }).catch(error => error);
  }

  signInWithGoogle() {
    const providerGoogle = new firebase.auth.GoogleAuthProvider();
    return this.afAuth.auth.signInWithPopup(providerGoogle)
      .then((result) => {
        return true;
      }).catch(error => error);
  }

  signInWithFacebook() {
    const providerFacebook = new firebase.auth.FacebookAuthProvider();
    return this.afAuth.auth.signInWithPopup(providerFacebook)
      .then((result) => {
        return true;
      }).catch(error => error);
  }

  signInWithTwitter() {
    const providerFacebook = new firebase.auth.TwitterAuthProvider();
    return this.afAuth.auth.signInWithPopup(providerFacebook)
      .then((result) => {
        return true;
      }).catch(error => error);
  }
/*
  sendEmailVerification(){
    console.log("message")
    let email :string ;
     this.afAuth.authState.subscribe((result) => {
      console.log("result",result)
      result.sendEmailVerification().then(() => {
        console.log('email sent');
      })
      return true;
    })
  }
  */
}
