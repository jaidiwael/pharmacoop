import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { FirestoreService } from '../../services/firestore.service';
import { StoreService } from '../../services/store.service';
import { UserService } from '../../services/user.service';
import { Store } from '../../classes/store.class';
import { Software } from "../../classes/software.class";
import { Observable, Subject, of } from 'rxjs';
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { } from '@types/googlemaps';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {

  public payPalConfig?: PayPalConfig;


  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  options={
    types: [],
    componentRestrictions: { country: 'CA' }
  };
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;
  location = new google.maps.LatLng(36.806496, 10.181532);

  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<any>();
  public dtOptions: DataTables.Settings = {};
  private store: Store = new Store();
  private storeInfo: Store = new Store();
  private listStore: Store[];
  private listEnvironnement: any[];
  private listSoftware: any[] = [];
  private listSelectedUidSoftware: any[] = [];
  private loading = false;
  private uidCurrentUser: string;
  private displayDetail: boolean = false;
  private titleModal: String = 'Ajouter un nouveau store';
  private titleButton: String = 'Ajouter';

  constructor(private storeService: StoreService,
    private firestoreService: FirestoreService,
    private userService: UserService) {
  }

  ngOnInit() {
    this.initConfig();
    this.uidCurrentUser = this.userService.getUidCurrentUser();
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };

    this.getListStore();
    this.getListEnvironnement();
    this.getListSoftware();
  }

  getListStore() {
    this.loading = true;
    this.storeService.getAllStore(this.uidCurrentUser).subscribe((data) => {
      this.listStore = data;
      this.dtTrigger.next();
      this.loading = false;
    });
  }

  rerender(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }

  getListEnvironnement() {
    this.firestoreService.getAllEnvironnement().subscribe((data) => {
      this.listEnvironnement = data;
    });
  }

  getEnvironnement(uidEnvironnement) {
    return this.listEnvironnement.find((x) => x.uid === uidEnvironnement);
  }


  getListSoftware() {
    this.firestoreService.getAllSoftware().subscribe((data) => {
      this.listSoftware = data;
    });
  }

  submitStore(storeForm) {
    const payload = {
      uidEnvironnement: storeForm.value.uidEnvironnement,
      environnement: this.getEnvironnement(storeForm.value.uidEnvironnement),
      name: storeForm.value.name,
      adress: storeForm.value.adress,
      city: storeForm.value.city,
      province: storeForm.value.province,
      postalCode: storeForm.value.postalCode,
      phone: storeForm.value.phone,
      description: storeForm.value.description,
      listSelectedSoftware: this.getListSoftwareWithUid()
    };
    console.log("payload", payload);
    if (storeForm.value.uid) {
      this.storeService.updateStore(this.uidCurrentUser, storeForm.value.uid, payload)
        .then(() => {
        })
    } else {
      this.storeService.addStore(this.uidCurrentUser, payload)
        .then(() => {
        })
    }
    this.resetForm();

  }

  onEdit(store: Store) {
    this.displayDetail = false;
    this.listSelectedUidSoftware = [];
    this.store = store;
    this.store.uidEnvironnement = store.environnement.uid;
    this.listSelectedUidSoftware = store.listSelectedSoftware.map(x => x.uid);
    this.titleModal = 'Modifier un store';
    this.titleButton = 'Modifier';
  }

  getListSoftwareWithUid() {
    const listSoftwareSelected: Software[] = [];
    for (const uidSoftware of this.listSelectedUidSoftware) {
      listSoftwareSelected.push(this.listSoftware.find((x) => x.uid === uidSoftware));
    }
    return listSoftwareSelected;
  }

  onDelete(store: Store) {
    if (confirm('Êtes-vous sûr de supprimer cet store?') === true) {
      this.storeService.removeStore(this.uidCurrentUser, store.uid)
        .then((data) => {
        });
    }
  }
  onDisplay(store: Store) {
    this.displayDetail = true;
    this.titleModal = 'Informations entreprise : ' + store.name;
    this.storeInfo = store;
  }
  resetForm() {
    this.store = new Store();
    this.listSelectedUidSoftware = [];
    this.displayDetail = false;
    this.titleModal = 'Ajouter un nouveau store';
    this.titleButton = 'Ajouter';
  }




  onChangeSoftware(software, isChecked: boolean) {
    if (isChecked) {
      this.listSelectedUidSoftware.push(software);
    } else {
      const index = this.listSelectedUidSoftware.indexOf(software);
      this.listSelectedUidSoftware.splice(index, 1);
    }
  }

  isSelectedSoftware(software) {
    return this.listSelectedUidSoftware.indexOf(software) >= 0;
  }

  private initConfig(): void {
    this.payPalConfig = new PayPalConfig(
      PayPalIntegrationType.ClientSideREST,
      PayPalEnvironment.Sandbox,
      {
        commit: true,
        client: {
          sandbox:
          'AS0lyAQXVf2gIYXsvxnVNZK8KgWmiZMHFTNKdr0xeNlIU29IZ1EGqE1sSk0y7H1i566sJKrLYOIguzl9'
        },
        button: {
          label: 'pay',
          layout: 'vertical',
          shape: 'rect',
        },
        onPaymentComplete: (data, actions) => {
          console.log('OnPaymentComplete');
        },
        onCancel: (data, actions) => {
          console.log('OnCancel');
        },
        onError: err => {
          console.log('OnError');
        },

        transactions: [
          {
            amount: {
              total: 30.11,
              currency: 'CAD'
            }
          }
        ]
      }
    );
  }

  public handleAddressChange(address) {
    console.log(address);
    console.log(address.geometry.location.lng());
    console.log(address.geometry.location.lat());
    console.log(address.geometry.location.toJSON());
    console.log(address.geometry.viewport.getNorthEast());
    this.location = address.geometry.location;
    /*this.map.setCenter(address.geometry.location);
    let marker = new google.maps.Marker({
      position: address.geometry.location,
      map: this.map,
      title: 'Got you!'
    });
    */
    this.ngAfterContentInit();
  }

  ngAfterContentInit() {

    let mapProp = {
      center: this.location,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);

    let marker = new google.maps.Marker({
      position: this.location,
      map: this.map,
      title: 'Got you!'
    });

  }

}
