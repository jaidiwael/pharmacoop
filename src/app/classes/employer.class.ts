export class Employer {
    uid?: string;
    verifyProfile?: Boolean;
    blockProfile?: Boolean;
    email?: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    gender?: string;
    positionHeld?: string;
    phoneNumber?: string;
    verifyNumberPhone?: Boolean;
    authorizedSmsEmail?: Boolean;
    validDocuments?: Boolean;

}