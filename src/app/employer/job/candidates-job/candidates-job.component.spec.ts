import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatesJobComponent } from './candidates-job.component';

describe('CandidatesJobComponent', () => {
  let component: CandidatesJobComponent;
  let fixture: ComponentFixture<CandidatesJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatesJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatesJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
