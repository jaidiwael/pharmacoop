export class Category {
    uid?: string;
    title?: string;
    orderNumber?: boolean;
}