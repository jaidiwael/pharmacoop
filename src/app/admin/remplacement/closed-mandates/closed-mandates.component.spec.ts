import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClosedMandatesComponent } from './closed-mandates.component';

describe('ClosedMandatesComponent', () => {
  let component: ClosedMandatesComponent;
  let fixture: ComponentFixture<ClosedMandatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClosedMandatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClosedMandatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
