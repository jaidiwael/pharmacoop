import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { MandatesComponent } from './remplacement/mandates/mandates.component';
import { PendingMandatesComponent } from './remplacement/pending-mandates/pending-mandates.component';
import { ConfirmedMandatesComponent } from './remplacement/confirmed-mandates/confirmed-mandates.component';
import { ClosedMandatesComponent } from './remplacement/closed-mandates/closed-mandates.component';
import { SearchJobComponent } from './job/search-job/search-job.component';
import { AppliedJobComponent } from './job/applied-job/applied-job.component';

const routes: Routes = [
    {
        path: 'employee', component: HomeComponent,
        children: [
            { path: 'login', component: LoginComponent },
            { path: 'register', component: RegisterComponent },
            { path: 'profile', component: ProfileComponent },
            { path: 'mandates', component: MandatesComponent },
            { path: 'pending-mandates', component: PendingMandatesComponent },
            { path: 'confirmed-mandates', component: ConfirmedMandatesComponent },
            { path: 'closed-mandates', component: ClosedMandatesComponent },
            { path: 'search-job', component: SearchJobComponent },
            { path: 'applied-job', component: AppliedJobComponent },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmployeeRoutingModule { }
