import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { StoreComponent } from './store/store.component';
import { MandatesComponent } from './remplacement/mandates/mandates.component';
import { ConfirmedMandatesComponent } from './remplacement/confirmed-mandates/confirmed-mandates.component';
import { ClosedMandatesComponent } from './remplacement/closed-mandates/closed-mandates.component';
import { CandidatesMandatesComponent } from './remplacement/candidates-mandates/candidates-mandates.component';
import { AdsJobComponent } from './job/ads-job/ads-job.component';
import { CandidatesJobComponent } from './job/candidates-job/candidates-job.component';
const routes: Routes = [
    {
        path: 'employer', component: HomeComponent,
        children: [
            { path: 'login', component: LoginComponent },
            { path: 'register', component: RegisterComponent },
            { path: 'profile', component: ProfileComponent },
            { path: 'store', component: StoreComponent },
            { path: 'mandates', component: MandatesComponent },
            { path: 'candidates-mandates/:uid', component: CandidatesMandatesComponent },
            { path: 'confirmed-mandates', component: ConfirmedMandatesComponent },
            { path: 'colsed-mandates', component: ClosedMandatesComponent },
            { path: 'ads-job', component: AdsJobComponent },
            { path: 'candidates-job/:uid', component: CandidatesJobComponent },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmployerRoutingModule { }
