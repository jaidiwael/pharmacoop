import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { JobService } from "../../../services/job.service";
import { UserService } from "../../../services/user.service";
import { DataTableDirective } from 'angular-datatables';
import { Ads } from "../../../classes/ads.class";

@Component({
  selector: 'app-shared-ads',
  templateUrl: './shared-ads.component.html',
  styleUrls: ['./shared-ads.component.scss']
})
export class SharedAdsComponent implements OnInit {

  @Input() adsStatus: any;

  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<any>();
  private dtOptions: DataTables.Settings = {};

  private listAds: Ads[] = [];
  private uidCurrentUser: string;
  private adsInfo: any;
  private loading: boolean = true;
  private titleModal: string = "";
  private displayDetail: Boolean = false;

  constructor(public userService: UserService,
    public jobService: JobService) {

  }
  ngOnInit() {
    console.log("this.adsStatus", this.adsStatus)
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getListAdsByCategory();
  }


  getListAdsByCategory() {
    this.userService.getEmployeeInfo().subscribe((user: any) => {
      this.uidCurrentUser = user.uid;
      this.jobService.getAllAdsByCategory(user.category.uid).subscribe((data: Ads[]) => {
        if (this.adsStatus.getAllAds) this.listAds = data.filter(
          x => x.listUidEmployeeParticipate.indexOf(this.uidCurrentUser) === -1);
        else this.listAds = data.filter(
          x => x.listUidEmployeeParticipate.indexOf(this.uidCurrentUser) !== -1);
        this.dtTrigger.next();
        this.loading = false;
      });
    });
  }

  rerender(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }
  applyToThisAd(ads: Ads) {
    if (confirm('Voulez-vous appliquez à cette annonce ?') === true) {
      ads.listUidEmployeeParticipate.push(this.uidCurrentUser);
      const payload: any = { listUidEmployeeParticipate: ads.listUidEmployeeParticipate }
      this.jobService.updateAds(ads.uid, payload);
      this.getListAdsByCategory();
      this.rerender();
    }

  }

  onDisplay(ads: Ads) {
    this.displayDetail = true;
    this.titleModal = 'Informations mandats : ' + ads.refAnnonce;
    this.adsInfo = ads;
  }
}