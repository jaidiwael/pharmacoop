import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { EmployerRoutingModule } from './employer.routing';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { StoreComponent } from './store/store.component';
import { MandatesComponent } from './remplacement/mandates/mandates.component';
import { ConfirmedMandatesComponent } from './remplacement/confirmed-mandates/confirmed-mandates.component';
import { CandidatesMandatesComponent } from './remplacement/candidates-mandates/candidates-mandates.component';
import { AdsJobComponent } from './job/ads-job/ads-job.component';
import { CandidatesJobComponent } from './job/candidates-job/candidates-job.component';
import { ClosedMandatesComponent } from './remplacement/closed-mandates/closed-mandates.component';

import { LoadingModule } from 'ngx-loading';
import { DataTablesModule } from 'angular-datatables';
import { MenuComponent } from './menu/menu.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ToastrModule } from 'ngx-toastr';
import { NgxMaskModule } from 'ngx-mask';
import { SharedModule } from "../core/shared-module/shared.module";
import { NgxPayPalModule } from 'ngx-paypal';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

@NgModule({
    declarations: [
        LoginComponent,
        HomeComponent,
        RegisterComponent,
        ProfileComponent,
        StoreComponent,
        MenuComponent,
        MandatesComponent,
        ConfirmedMandatesComponent,
        AdsJobComponent,
        CandidatesJobComponent,
        CandidatesMandatesComponent,
        ClosedMandatesComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        EmployerRoutingModule,
        HttpModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        LoadingModule,
        DataTablesModule,
        RouterModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({ timeOut: 3000 }),
        NgxMaskModule.forRoot(),
        SharedModule,
        NgxPayPalModule,
        GooglePlaceModule


    ],
})
export class EmployerModule { }
