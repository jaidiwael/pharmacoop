import { Component, OnInit, } from '@angular/core';

@Component({
  selector: 'app-pending-mandates',
  templateUrl: './pending-mandates.component.html',
  styleUrls: ['./pending-mandates.component.scss']
})
export class PendingMandatesComponent implements OnInit {

  public payload: object = {
    statusMandate: 'free',
    owner: 'employee',
    method: '',
    parentLink:'employee/mandates',
    titlePage: 'Mandats en attente',
  }
  constructor() { }
  ngOnInit() { }

}
