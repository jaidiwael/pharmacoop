import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from "../../services/user.service";
import { User } from "../../classes/user.class";
import { Observable, Subject, of } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<any>();
  private dtOptions: DataTables.Settings = {};
  private loading = false;
  private displayDetail: boolean = false;
  private titleModal: String = 'Detail employer';
  private titleButton: String = 'Ajouter';
  private employeeInfo: any;
  private listEmployee: any[];
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getAllListValidEmployee();
  }
  getAllListValidEmployee() {
    this.userService.getAllValidEmployee().subscribe((data: any) => {
      this.listEmployee = data;
      this.dtTrigger.next();
      this.loading = false;
      console.log('this.listEmployer', this.listEmployee);
    }, error => {
      console.log("error", error);
    })
  }
  rerender(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }

  onDisplay(employee: any) {
    console.log("employee", employee);
    this.displayDetail = true;
    this.employeeInfo = employee;

  }

}




