import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-confirmed-mandates',
  templateUrl: './confirmed-mandates.component.html',
  styleUrls: ['./confirmed-mandates.component.scss']
})
export class ConfirmedMandatesComponent implements OnInit {
 public payload : object =  {
    statusMandate : 'confirmed' , 
    owner : 'employer',
    method:'getListConfirmedMandateByEmployer',
    parentLink:'employer/mandates',
    titlePage: 'Mes mandats confirmés',
  }
  constructor() { }

  ngOnInit() { }
  
 
}