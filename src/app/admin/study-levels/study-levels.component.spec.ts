import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyLevelsComponent } from './study-levels.component';

describe('StudyLevelsComponent', () => {
  let component: StudyLevelsComponent;
  let fixture: ComponentFixture<StudyLevelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyLevelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyLevelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
