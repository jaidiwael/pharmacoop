import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from "../../services/user.service";
import { User } from "../../classes/user.class";
import { Observable, Subject, of } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-employer',
  templateUrl: './employer.component.html',
  styleUrls: ['./employer.component.scss']
})
export class EmployerComponent implements OnInit {

  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<any>();
  public dtOptions: DataTables.Settings = {};
  private listEmployer: any[];
  private loading = false;
  private displayDetail: boolean = false;
  private titleModal: String = 'Detail employer';
  private titleButton: String = 'Ajouter';
  private employerInfo: any;

  constructor(private userService: UserService) { }


  ngOnInit() {
    console.log("message");
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getAllListValidEmployer();
  }

  getAllListValidEmployer() {
    this.userService.getAllValidEmployer().subscribe((data: any) => {
      this.listEmployer = data;
      this.dtTrigger.next();
      this.loading = false;
      console.log('this.listEmployer', this.listEmployer);
    }, error => {
      console.log("error", error);
    })
  }
  rerender(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }


  onDisplay(employer: any) {
    this.displayDetail = true;
    this.titleModal = 'Informations Employers';
    this.employerInfo = employer;

  }
  onDelete(employer: any) {
    if (confirm('Êtes-vous sûr de supprimer cette employer?') === true) {
      console.log("employer", employer);
      this.userService.removeEmployer(employer.id);
      this.getAllListValidEmployer();
      this.rerender();
    }
  }

}
