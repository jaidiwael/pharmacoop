import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Ads } from "../classes/ads.class";
@Injectable({
  providedIn: 'root'
})
export class JobService {
  private adsCollection: any;
  constructor(private afs: AngularFirestore) {
    this.adsCollection = this.afs.collection('ads');
  }

  addAds(ads: any) {
    return this.adsCollection.add(ads);
  }

  removeAds(uid: String) {
    return this.adsCollection.doc(uid).delete();
  }

  updateAds(uid: String, ads: any) {
    return this.adsCollection.doc(uid).update(ads);
  }
  getAllAdsByUser(uidUser: String) {
    return this.afs.collection('ads',
      ref => ref.where('uidEmployer', '==', uidUser))
      .snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const uid = a.payload.doc.id;
        return { uid, ...data };
      }))
      );
  }
  getAdsByUid(uidJob: string) {
    return this.afs.collection('ads').doc(uidJob).valueChanges();

  }
  getAllAds() {
    return this.afs.collection('ads')
      .snapshotChanges().pipe(
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const uid = a.payload.doc.id;
        return { uid, ...data };
      }))
      );
  }

  getAllAdsByCategory(uidCategory: String) {
    return this.afs.collection('ads', ref => ref.where('category.uid', '==', uidCategory))
      .snapshotChanges().pipe(map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const uid = a.payload.doc.id;
        return { uid, ...data };
      }))
      );
  }
}














