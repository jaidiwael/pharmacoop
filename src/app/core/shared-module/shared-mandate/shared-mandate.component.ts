import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Observable, Subject, of } from 'rxjs';
import { Store } from "../../../classes/store.class";
import { Router } from '@angular/router';
import { Mandates } from '../../../classes/mandates.class';
import { FirestoreService } from '../../../services/firestore.service';
import { StoreService } from '../../../services/store.service';
import { MandatesService } from '../../../services/mandates.service';
import { UserService } from '../../../services/user.service';
@Component({
  selector: 'app-shared-mandate',
  templateUrl: './shared-mandate.component.html',
  styleUrls: ['./shared-mandate.component.scss']
})
export class SharedMandateComponent implements OnInit {

  @Input() payloadMandate: any;

  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<any>();
  public dtOptions: DataTables.Settings = {};

  private UserInfo: any;
  private displayEmployee: boolean = false;
  private displayInfoMandate: boolean = false;
  private displayFormMandate: boolean = true;
  private mandates: Mandates = new Mandates();

  private mandatesInfo: Mandates = new Mandates();
  private listMandates: any[] = [];
  private displayDetail: Boolean = false;
  private titleModal: string = 'Ajouter une nouvelle mandats';
  private titleButton: string = 'Ajouter';
  private min = new Date();
  private store: Store = new Store();
  private listStore: any[];
  private environnementName: string = "";
  private listSoftware: any[] = [];
  private listSelectedUidSoftware: any[] = [];
  private listSelectedCurrentUidSoftware = [];
  private uidCurrentStore: any;
  private listCategory: any[];
  private uidCurrentUser: string;
  private loading = false;

  constructor(private router: Router,
    private storeService: StoreService,
    private firestoreService: FirestoreService,
    private mandatesService: MandatesService,
    private userService: UserService) {

    this.uidCurrentUser = this.userService.getUidCurrentUser();
    console.log("this.uidCurrentUser", this.uidCurrentUser);

  }
  ngOnInit() {
    console.log("payload", this.payloadMandate);
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getListMandates();
    this.getListCategory();
    this.getListStore();
  }

  getListMandates() {

    switch (this.payloadMandate.owner) {
      case "employer": {
        this.getListMandatesByEmployer();
        break;
      }
      case "admin": {
        this.mandatesService.getAllMandates(this.payloadMandate.statusMandate)
          .subscribe((data) => {
            this.listMandates = data;
            this.dtTrigger.next();
            this.loading = false;
          });
      }
      case "employee": {
        this.getListMandatesByCategory();
        break;
      }
      default: {
        console.log("error")
        break;
      }
    }
  }

  getListMandatesByCategory() {
    this.userService.getEmployeeInfo().subscribe((user: any) => {
      this.mandatesService.getAllMandatesByCategory(user.category.uid, this.payloadMandate.statusMandate).subscribe((data: Mandates[]) => {
        if (this.payloadMandate.method === "applyToThisMandate")
          this.listMandates = data.filter(x => x.listUidEmployeeParticipate.indexOf(this.uidCurrentUser) === -1);
        else
          this.listMandates = data.filter(x => x.listUidEmployeeParticipate.indexOf(this.uidCurrentUser) !== -1);
        this.dtTrigger.next();
        this.loading = false;
      });
    });
  }

  getListMandatesByEmployer() {
    this.mandatesService.getAllMandatesByEmployer(this.uidCurrentUser, this.payloadMandate.statusMandate)
      .subscribe((data) => {
        this.listMandates = data;
        this.dtTrigger.next();
        this.loading = false;
      });
  }

  rerender(): void {
    console.log("render");
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }

  getListStore() {
    this.storeService.getAllStore(this.uidCurrentUser).subscribe((data) => {
      this.listStore = data;
    });
  }

  getListCategory() {
    this.firestoreService.getAllCategroy().subscribe((data) => {
      this.listCategory = data;
    });
  }

  submitMandates(mandatesForm) {
    const payload: Mandates = {
      uidEmployer: this.uidCurrentUser,
      publish: false,
      category: this.listCategory.find((x) => x.uid === mandatesForm.value.uidCategory),
      store: this.listStore.find((x) => x.uid === mandatesForm.value.uidStore),
      startDate: mandatesForm.value.rangeDate[0],
      endDate: mandatesForm.value.rangeDate[1],
      nbHour: this.getNbHourFromRangeDate(mandatesForm.value.rangeDate),
      pricePerHour: mandatesForm.value.pricePerHour,
      listSoftware: this.getListSoftwareWithUid(),
      total: this.getTotalPrice(this.getNbHourFromRangeDate(mandatesForm.value.rangeDate), mandatesForm.value.pricePerHour)
    };
    if (mandatesForm.value.uid) {
      this.mandatesService.updateMandates(mandatesForm.value.uid, payload);
    } else {
      payload.MandateStatus = "free";
      payload.uidSelectedEmployee = "";
      payload.listUidEmployeeParticipate = []
      payload.refMandates = 'REM-' + Math.random().toString(36).substr(2, 10).toUpperCase();
      payload.createdDate = new Date();
      console.log("payload", payload);
      this.mandatesService.addMandates(payload);
      this.listSoftware = [];
      this.listSelectedUidSoftware = [];
      this.getListMandatesByEmployer();
      this.rerender();
    }
    this.resetForm();

  }

  onEdit(mandates: Mandates) {
    this.listSelectedUidSoftware = [];
    this.displayEmployee = false;
    this.displayInfoMandate = false;
    this.displayFormMandate = true;
    this.mandates = mandates;
    this.mandates.rangeDate = this.getDateFromDatePicker(this.mandates.startDate, this.mandates.endDate);
    this.mandates.uidCategory = mandates.category.uid;
    this.mandates.uidStore = mandates.store.uid;
    this.uidCurrentStore = mandates.store.uid;
    this.environnementName = mandates.store.environnement.title;
    this.listSelectedUidSoftware = mandates.listSoftware.map(x => x.uid);
    this.listSelectedCurrentUidSoftware = mandates.listSoftware.map(x => x.uid);
    this.listSoftware = mandates.store.listSelectedSoftware;
    this.titleModal = 'Modifier une mandats';
    this.titleButton = 'Modifier';
  }

  onDelete(mandates: Mandates) {
    if (confirm('Êtes-vous sûr de supprimer cet mandates?') === true) {
      this.mandatesService.removeMandates(mandates.uid);
    }
  }

  onPublish(mandate) {
    if (confirm('Êtes-vous sûr de publier cet mandat?') === true) {
      const payload = { publish: true }
      this.mandatesService.updateMandates(mandate.uid, payload);
    }
  }
  onDisplay(mandate: Mandates) {
    this.displayEmployee = false;
    this.displayInfoMandate = true;
    this.displayFormMandate = false;
    this.titleModal = 'Informations mandats : ' + mandate.refMandates;
    this.mandatesInfo = mandate;
  }

  resetForm() {
    this.mandates = new Mandates();
    this.displayEmployee = false;
    this.displayInfoMandate = false;
    this.displayFormMandate = true;
    this.titleModal = 'Ajouter une nouvelle mandats';
    this.titleButton = 'Ajouter';
    this.environnementName = "";
    this.listSelectedUidSoftware = [];
    this.listSoftware = [];
    this.uidCurrentStore = '';
  }

  getNbHourFromRangeDate(rangeDate) {
    const startDate: any = rangeDate[0];
    const endDate: any = rangeDate[1];
    return Math.abs(startDate - endDate) / 36e5;
  }

  getTotalPrice(nbHour, price) {
    return nbHour * parseFloat(price);
  }

  onChangeStore(uidStore) {
    this.listSelectedUidSoftware = [];
    this.listSoftware = [];
    if (this.uidCurrentStore === uidStore) this.listSelectedUidSoftware = this.listSelectedCurrentUidSoftware;
    const selectedStore = this.listStore.find((x) => x.uid === uidStore);
    this.listSoftware = selectedStore.listSelectedSoftware;
    this.environnementName = selectedStore.environnement.title;
  }

  onChangeSoftware(software, isChecked: boolean) {
    if (isChecked) {
      this.listSelectedUidSoftware.push(software);
    } else {
      const index = this.listSelectedUidSoftware.indexOf(software);
      this.listSelectedUidSoftware.splice(index, 1);
    }
  }

  isSelectedSoftware(software) {
    return this.listSelectedUidSoftware.indexOf(software) >= 0;
  }

  getListSoftwareWithUid() {
    const listSoftwareSelected: any[] = [];
    for (const uidSoftware of this.listSelectedUidSoftware) {
      listSoftwareSelected.push(this.listSoftware.find((x) => x.uid === uidSoftware));
    }
    return listSoftwareSelected;
  }

  getDateFromDatePicker(startDate, endDate) {
    let dateTimeRange: Date[] = [startDate.toDate(), endDate.toDate()];
    return dateTimeRange;
  }


  applyToThisMandate(mandate: any) {
    if (confirm('Voulez-vous appliquez à ce mandat ?') === true) {
      mandate.listUidEmployeeParticipate.push(this.uidCurrentUser);
      const payload: any = { listUidEmployeeParticipate: mandate.listUidEmployeeParticipate }
      this.mandatesService.updateMandates(mandate.uid, payload);
      this.getListMandatesByCategory();
      this.rerender();
    }

  }

  onDisplayEmployer(mandates: Mandates) {
    this.userService.getEmployerInfoByUid(mandates.uidEmployer).subscribe((user: any) => {
      this.displayEmployee = true;
      this.displayInfoMandate = false;
      this.displayFormMandate = false;
      this.titleModal = 'Informations Employeer';
      this.UserInfo = user;
    }, error => {
      console.log('Error', error);
    })


  }

  onDisplayEmployee(mandates: Mandates) {
    this.userService.getEmployeeInfoByUid(mandates.uidSelectedEmployee).subscribe((user: any) => {
      this.displayEmployee = true;
      this.displayInfoMandate = false;
      this.displayFormMandate = false;
      this.titleModal = 'Informations Employee';
      this.UserInfo = user;
    }, error => {
      console.log('Error', error);
    })


  }

  goToCandidatePage(mandate) {
    console.log(mandate);
    this.router.navigate(['/' + this.payloadMandate.owner + '/candidates-mandates', mandate.uid]);
  }

  formatDate(date: Date): string {
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    const hour = date.getHours();
    const minute = date.getMinutes();

    return `${day}/${month}/${year} ${hour}:${minute}`;
  }

}
