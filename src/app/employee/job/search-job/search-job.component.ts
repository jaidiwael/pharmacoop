import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-job',
  templateUrl: './search-job.component.html',
  styleUrls: ['./search-job.component.scss']
})
export class SearchJobComponent implements OnInit {
  
  public adsInfo: any ={
    owner : 'employee',
    getAllAds: true,
    parentLink:'employee/search-job',
    titlePage:'Liste des annonces',
  } 
  constructor() {

  }
  ngOnInit() {
  }

}
