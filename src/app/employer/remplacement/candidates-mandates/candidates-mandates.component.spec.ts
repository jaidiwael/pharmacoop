import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatesMandatesComponent } from './candidates-mandates.component';

describe('CandidatesMandatesComponent', () => {
  let component: CandidatesMandatesComponent;
  let fixture: ComponentFixture<CandidatesMandatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatesMandatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatesMandatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
