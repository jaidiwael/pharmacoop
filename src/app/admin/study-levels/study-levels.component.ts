import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { FirestoreService } from '../../services/firestore.service';
import { Observable, Subject, of } from 'rxjs';
import {StudyLevel} from "../../classes/study-levels.class" ;
@Component({
  selector: 'app-study-levels',
  templateUrl: './study-levels.component.html',
  styleUrls: ['./study-levels.component.scss'],
  providers: [FirestoreService]
})
export class StudyLevelsComponent implements OnInit {

  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<StudyLevel>();
  private listStudyLevels: StudyLevel[];
  private studyLevelsForm: FormGroup;
  private loading = true;
  private titleModal = 'Ajouter un nouveau Niveau';
  private titleButton = 'Ajouter';

  dtOptions: DataTables.Settings = {};

  constructor(private formBuilder: FormBuilder,
    private firestoreService: FirestoreService) {

    this.createForm();
  }

  ngOnInit() {
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getAllStudyLevels();
  }

  rerender(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }

  createForm() {
    this.studyLevelsForm = this.formBuilder.group({
      title: ['', Validators.required],
      uid: ['']
    });
  }

  getAllStudyLevels() {
    this.firestoreService.getAllStudyLevels().subscribe((data) => {
      this.listStudyLevels = data.reverse();
      this.dtTrigger.next();
      this.loading = false;
    });

  }

  submitStudyLevels(studyLevelsForm) {
    const payload = { title: studyLevelsForm.value.title };
    if (studyLevelsForm.value.uid) {
      this.firestoreService.updateStudyLevels(payload, studyLevelsForm.value.uid); 
      this.getAllStudyLevels(); 
      this.rerender() ;
    } else {
      this.firestoreService.addStudyLevels(payload);
      this.getAllStudyLevels(); 
      this.rerender() ;
    }
    this.resetForm();

  }

  onEdit(studyLevels) {
    this.titleModal = 'Modifier un niveau';
    this.titleButton = 'Modifier';
    this.studyLevelsForm = this.formBuilder.group({
      title: [studyLevels.title, Validators.required],
      uid: [studyLevels.uid]
    });
  }

  onDelete(studyLevels) {
    if (confirm('Êtes-vous sûr de supprimer cet niveau?') === true) {
      this.firestoreService.removeStudyLevels(studyLevels.uid)
           this.getAllStudyLevels();
           this.rerender();

    
    }
  }


  resetForm() {
    this.studyLevelsForm.reset();
    this.titleModal = 'Ajouter un nouveau studyLevels';
    this.titleButton = 'Ajouter';
  }
}
