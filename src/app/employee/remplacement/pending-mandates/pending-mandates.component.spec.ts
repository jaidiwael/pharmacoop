import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingMandatesComponent } from './pending-mandates.component';

describe('PendingMandatesComponent', () => {
  let component: PendingMandatesComponent;
  let fixture: ComponentFixture<PendingMandatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingMandatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingMandatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
