import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { StoreService } from '../../../services/store.service';
import { MandatesService } from '../../../services/mandates.service';
import { UserService } from '../../../services/user.service';
import { Mandates } from '../../../classes/mandates.class';
import { DataTableDirective } from 'angular-datatables';
import { FirestoreService } from '../../../services/firestore.service';
import { JobService } from "../../../services/job.service";
import { Ads } from "../../../classes/ads.class";

@Component({
  selector: 'app-ads-job',
  templateUrl: './ads-job.component.html',
  styleUrls: ['./ads-job.component.scss'],

})
export class AdsJobComponent implements OnInit {
  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<any>();
  public dtOptions: DataTables.Settings = {};

  private ads: Ads = new Ads();
  private adsInfo: Ads = new Ads();
  private listAds: any[];
  private loading = false;
  private titleModal: String = "";
  private displayAds: boolean = false;
  private displayUser: boolean = false;
  private UserInfo: any;

  constructor(private jobService: JobService,
    private storeService: StoreService,
    private firestoreService: FirestoreService,
    private userService: UserService
  ) {

  }

  ngOnInit() {
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };

    this.getListAds()

  }
  getListAds() {
    this.jobService.getAllAds().subscribe((data: any) => {
      this.listAds = data;
      console.log(this.listAds); 
      this.dtTrigger.next();
      this.loading = false;
    })
  }


  onDisplay(ads: Ads) {
    this.displayUser = false;
    this.displayAds = true;
    this.titleModal = 'Informations annonce';
    this.adsInfo = ads;
  }


  onDisplayEmployer(ads: Ads) {
    this.userService.getEmployerInfoByUid(ads.uidEmployer).subscribe((user: any) => {
      this.displayAds = false;
      this.displayUser = true;
      this.titleModal = 'Informations Employeer';
      this.UserInfo = user;
    })

  }

}



