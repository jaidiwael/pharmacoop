import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { FirestoreService } from '../../services/firestore.service';
import { Observable, Subject, of } from 'rxjs';
import {Software} from "../../classes/software.class";
@Component({
  selector: 'app-software',
  templateUrl: './software.component.html',
  styleUrls: ['./software.component.scss'],
  providers: [FirestoreService]
})
export class SoftwareComponent implements OnInit {

  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<Software>();
  private listSoftware: Software[];

  private softwareForm: FormGroup;
  private loading = true;
  private titleModal = 'Ajouter un nouveau software';
  private titleButton = 'Ajouter';

  dtOptions: DataTables.Settings = {};

  constructor(private formBuilder: FormBuilder,
    private firestoreService: FirestoreService) {

    this.createForm();
  }

  ngOnInit() {
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getAllSoftware();
  }

  rerender(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }

  createForm() {
    this.softwareForm = this.formBuilder.group({
      title: ['', Validators.required],
      uid: ['']
    });
  }

  getAllSoftware() {
    this.firestoreService.getAllSoftware().subscribe((data) => {
      this.listSoftware = data.reverse();
      this.dtTrigger.next();
      this.loading = false;
    });

  }

  submitSoftware(softwareForm) {
    const payload = { title: softwareForm.value.title };
    if (softwareForm.value.uid) {
      this.firestoreService.updateSoftware(payload, softwareForm.value.uid)
      this.getAllSoftware() ; 
      this.rerender()
    } else {
      this.firestoreService.addSoftware(payload)
      this.getAllSoftware() ; 
      this.rerender()
    }
    this.resetForm();

  }

  onEdit(software) {
    this.titleModal = 'Modifier un software';
    this.titleButton = 'Modifier';
    this.softwareForm = this.formBuilder.group({
      title: [software.title, Validators.required],
      uid: [software.uid]
    });
  }

  onDelete(software) {
    if (confirm('Êtes-vous sûr de supprimer cet software?') === true) {
      this.firestoreService.removeSoftware(software.uid)
        this.getAllSoftware() ; 
        this.rerender()
      }
  }


  resetForm() {
    this.softwareForm.reset();
    this.titleModal = 'Ajouter un nouveau software';
    this.titleButton = 'Ajouter';
  }
}

