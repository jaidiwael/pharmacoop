import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedMandateComponent } from './shared-mandate.component';

describe('SharedMandateComponent', () => {
  let component: SharedMandateComponent;
  let fixture: ComponentFixture<SharedMandateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedMandateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedMandateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
