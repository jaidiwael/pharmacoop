import { Injectable } from '@angular/core';
import { Router } from '@angular/router'; 
@Injectable({
  providedIn: 'root'
})
export class NavbarService {

  public visible: boolean;

  constructor(public router : Router) {
    this.visible = false;
  }

  hide() {
    this.visible = false;
  }

  show() {
    this.visible = true;
  }

  logout(path){
    this.router.navigate([path]);
    this.hide() ; 
  }

  
}
