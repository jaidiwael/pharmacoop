import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { StoreService } from '../../../services/store.service';
import { MandatesService } from '../../../services/mandates.service';
import { UserService } from '../../../services/user.service';
import { Mandates } from '../../../classes/mandates.class';
import { DataTableDirective } from 'angular-datatables';
import { FirestoreService } from '../../../services/firestore.service';
import { JobService } from "../../../services/job.service";
import { Ads } from "../../../classes/ads.class";
import { Store } from '../../../classes/store.class';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-ads-job',
  templateUrl: './ads-job.component.html',
  styleUrls: ['./ads-job.component.scss'],
  providers: [DatePipe]
})
export class AdsJobComponent implements OnInit {
  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<any>();
  public  dtOptions: DataTables.Settings = {};
  private ads: Ads = new Ads();
  private adsInfo: Ads = new Ads();
  private listads: any[];
  private listCategory: any[];
  private loading = false;
  private uidCurrentUser: String;
  private store: Store = new Store();
  private storeInfo: Store = new Store();
  private listStore: Store[];
  private environnementName: string = ""
  private softwareList: any[] = [];
  private listSelectedUidSoftware: any[] = [];
  private displayDetail: Boolean = false;
  private titleModal: String = "jouter une nouvelle annonce d'emploi";
  private titleButton: String = 'Ajouter';
  private listSelectedCurrentUidSoftware = [];
  private uidCurrentStore: any;
  public  min = new Date();
  private ListePoste: any[] = [];
  private listeYearsExprence: object = [];
  constructor(private jobService: JobService,
    private storeService: StoreService,
    private firestoreService: FirestoreService,
    private userService: UserService,
    private datePipe: DatePipe) { }

  ngOnInit() {
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.uidCurrentUser = this.userService.getUidCurrentUser();
    this.getListeAds()
    this.getListCategory();
    this.getListStore();
    this.getAllUsersExperience();
    this.getAllUsersPostes();
  }
  getListeAds() {
    this.jobService.getAllAdsByUser(this.uidCurrentUser).subscribe((data: any) => {
      this.listads = data;
      this.dtTrigger.next();
      this.loading = false;
    })
  }


  getListStore() {
    this.storeService.getAllStore(this.uidCurrentUser).subscribe((data) => {
      this.listStore = data;
    });
  }

  submitAds(adsForm) {
    const payload: Ads = {
      uidEmployer: this.uidCurrentUser,
      startDate: adsForm.value.startDate,
      detail: adsForm.value.detail,
      category: this.getCategoryWithUid(adsForm.value.uidCategory),
      store: this.getStoreWithUid(adsForm.value.uidStore),
      yearsExprence: adsForm.value.yearsExprence,
      post: adsForm.value.post,
      softwareList: this.getListSoftwareWithUid(this.listSelectedUidSoftware)
    }
    if (adsForm.value.uid) {
      this.jobService.updateAds(adsForm.value.uid, payload);
    } else {
      payload.refAnnonce = 'EMP-' + Math.random().toString(36).substr(2,10).toUpperCase();
      payload.createdDate = new Date().toLocaleString();
      payload.listUidEmployeeParticipate=[]
      this.jobService.addAds(payload);
    }
    this.resetForm();

  }
  getStoreWithUid(uidStore) {
    let storeSelcted: object;
    for (let store of this.listStore) {
      if (store.uid === uidStore) {
        return store;
      }
    }
  }

  getCategoryWithUid(uidCategory) {
    let category: object;
    for (let category of this.listCategory) {
      if (category.uid === uidCategory) {
        return category;
      }
    }
  }

  onChangeStore(storeUid) {
    this.listSelectedUidSoftware = []
    if (this.uidCurrentStore === storeUid) this.listSelectedUidSoftware = this.listSelectedCurrentUidSoftware;
    let storeSlected: any;
    for (let store of this.listStore) {
      if (store.uid === storeUid) {
        storeSlected = store;
      }
    }
    this.softwareList = storeSlected.listSelectedSoftware;
    this.environnementName = storeSlected.environnement.title;
  }

  getListCategory() {
    this.firestoreService.getAllCategroy().subscribe((data) => {
      this.listCategory = data;
    });
  }

  getCategory(uidCategory) {
    return this.listCategory.find((x) => x.uid === uidCategory);
  }

  onEdit(ads: Ads) {
    this.listSelectedUidSoftware = [];
    this.displayDetail = false;
    this.ads = ads;
    this.ads.uidCategory = ads.category.uid;
    this.ads.uidStore = ads.store.uid;
    this.uidCurrentStore = ads.store.uid;
    this.listSelectedUidSoftware = ads.softwareList.map(x => x.uid);
    this.listSelectedCurrentUidSoftware = ads.softwareList.map(x => x.uid);
    this.softwareList = ads.store.listSelectedSoftware;
    this.ads.startDate = this.getFormatDatePicker(this.ads.startDate.seconds);
    this.titleModal = 'Modifier une annonce';
    this.titleButton = 'Modifier';
  }
  onDelete(ads: any) {
    if (confirm('Êtes-vous sûr de supprimer cet annonce?') === true) {
      this.jobService.removeAds(ads.uid);
    }
  }
  onDisplay(ads: Ads) {
    this.displayDetail = true;
    this.titleModal = 'Informations annonce';
    this.adsInfo = ads;
  }
  resetForm() {
    this.ads = new Ads();
    this.displayDetail = false;
    this.titleModal = 'Ajouter une nouvelle annonce';
    this.titleButton = 'Ajouter';
    this.environnementName = "";
    this.listSelectedUidSoftware = [];
    this.listSelectedCurrentUidSoftware = [];
    this.softwareList = [];
  }
  onChangeSoftware(software, isChecked: boolean) {
    if (isChecked) {
      this.listSelectedUidSoftware.push(software);
    } else {
      const index = this.listSelectedUidSoftware.indexOf(software);
      this.listSelectedUidSoftware.splice(index, 1);
    }
  }
  isSelectedSoftware(software) {
    return this.listSelectedUidSoftware.indexOf(software) >= 0;
  }
  getListSoftwareWithUid(listSelectedUidSoftware) {
    const listSoftwareSelected: any[] = [];
    for (const uidSoftware of listSelectedUidSoftware) {
      listSoftwareSelected.push(this.softwareList.find((x) => x.uid === uidSoftware));
    }
    return listSoftwareSelected;
  }
  getFormatDatePicker(date) {
    let currentDate = new Date(date * 1000);
    const day = currentDate.getDate();
    const monthIndex = currentDate.getMonth();
    const year = currentDate.getFullYear();
    let   formDate = new Date(year, monthIndex, day);
    return formDate;
  }
  getAllUsersExperience() {
    this.firestoreService.getAllExperience().subscribe((data: any) => {
      this.listeYearsExprence = data.sort((a, b) => (
        a.title > b.title ? 1 : -1));
    });
  }
  getAllUsersPostes() {
    this.firestoreService.getAllFormEmployment().subscribe((data: any) => {
      this.ListePoste = data;
    });
  }

}



