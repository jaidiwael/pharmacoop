import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { Observable, Subject, of } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from "../../classes/user.class";
import { ToastrService } from 'ngx-toastr';
import { NavbarService } from "../../services/navbar.service";
import { Employer } from '../../classes/employer.class'
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [UserService]
})
export class ProfileComponent implements OnInit {

  private userForm: FormGroup;
  public user: any;
  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private toastrService: ToastrService,
    public navbarService: NavbarService,
    private userService: UserService) {
    this.navbarService.show();
    this.userService.getEmployerInfo().subscribe((user: any) => {
      this.user = user;
    });
  }
  ngOnInit() {
    this.userService.getInfoCurrenEmployers().then((userObservable) => {
      userObservable.subscribe((user: Employer) => {
        this.createForm(user);
      });
    });
  }

  createForm(user) {
    this.userForm = this.formBuilder.group({
      email: [{ value: user.email , disabled: true }, Validators.required],
      gender: [user.gender, Validators.required],
      firstName: [user.firstName, Validators.required],
      lastName: [user.lastName, Validators.required],
      positionHeld: [user.positionHeld, Validators.required],
      phone: [user.phone, Validators.required],
      authorizedSmsEmail: [user.authorizedSmsEmail],
      validDocuments: [user.validDocuments],
    });
  }

  submitUser(userForm) {
    const payload = {
      firstName: userForm.value.firstName,
      lastName: userForm.value.lastName,
      gender: userForm.value.gender,
      positionHeld: userForm.value.positionHeld,
      phone: userForm.value.phone,
      authorizedSmsEmail: userForm.value.phone,
      validDocuments: userForm.value.phone,
    };

    this.userService.updateEmployer(payload)
      .then(data => {
        this.toastrService.success('les infos sont bien enregistré');
      });

  }

}
