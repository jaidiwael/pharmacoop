import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subject, of } from 'rxjs';
import { Router } from '@angular/router';
import { Category } from "../../classes/category.class";
import { Software } from "../../classes/software.class";
import { praticalEnvironnement } from "../../classes/pratical-environnement"
import { StudyLevel } from "../../classes/study-levels.class";
import { UserService } from '../../services/user.service';
import { FirestoreService } from '../../services/firestore.service';
import { NavbarService } from "../../services/navbar.service";
import { ToastrService } from 'ngx-toastr';
import { UploadFileServiceService } from "../../services/upload-file-service.service";
import { FileUpload } from "../../classes/fileupload.class"
import { Employee } from '../../classes/employee.class'
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [UserService]
})
export class ProfileComponent implements OnInit {
  public hiddenOrderNumber: boolean = false;
  private user: Employee = {};
  private uidCurrentUser: string;
  private listCategory: any[] = [];

  private listSoftware: Software[] = [];
  private listSelectedUidSoftware: any[] = [];

  private listEnvironnement: praticalEnvironnement[];
  private listSelectedUidEnvironnement: any[] = [];

  private listStudyLevel: StudyLevel[];
  private listSelectedUidstudyLevel: any[] = [];

  private listeYearsExprence: any[] = [];
  private selectedFileCv: FileList;
  private selectedFileCin: FileList;
  private selectedFileLicense: FileList;
  private currentFileUpload: FileUpload;

  constructor(
    private uploadFileServiceService: UploadFileServiceService,
    private router: Router,
    private userService: UserService,
    private toastrService: ToastrService,
    private firestoreService: FirestoreService,
    public navbarService: NavbarService) {

    this.navbarService.show();

  }

  ngOnInit() {
    this.getListCategory();
    this.getListSoftware();
    this.getListEnvironnement();
    this.getAllStudyLevels();
    this.getAllUsersExperience();

  }

  getInfoUser() {
    this.userService.getInfoCurrenEmployees().then((userObservable) => {
      userObservable.subscribe((user: Employee) => {
        this.user = user;
        console.log("this.user", this.user);
        this.uidCurrentUser = this.user.uid;
        if (user.category) {
          user.uidCategory = user.category.uid;
          this.onChangeCategory(user.category.uid);
        }
        if (user.listSoftware.length !== 0)
          this.listSelectedUidSoftware = user.listSoftware.map(x => x.uid);
        if (user.listStudyLevel.length !== 0)
          this.listSelectedUidstudyLevel = user.listStudyLevel.map(x => x.uid);
        if (user.listEnvironnement.length !== 0)
          this.listSelectedUidEnvironnement = user.listEnvironnement.map(x => x.uid);
      });
    });
  }

  getListCategory() {
    this.firestoreService.getAllCategroy().subscribe((data: any) => {
      this.listCategory = data;
      this.getInfoUser();
    });
  }

  onChangeCategory(uidCategory) {
    if (uidCategory) this.hiddenOrderNumber = this.listCategory
      .find(obj => obj.uid === uidCategory).orderNumber;
  }

  getListSoftware() {
    this.firestoreService.getAllSoftware().subscribe((data) => {
      this.listSoftware = data;
    });
  }

  onChangeSoftware(software, isChecked: boolean) {
    if (isChecked) {
      this.listSelectedUidSoftware.push(software);
    } else {
      const index = this.listSelectedUidSoftware.indexOf(software);
      this.listSelectedUidSoftware.splice(index, 1);
    }
  }

  isSelectedSoftware(software) {
    return this.listSelectedUidSoftware.indexOf(software) >= 0;
  }

  getListEnvironnement() {
    this.firestoreService.getAllEnvironnement().subscribe((data) => {
      this.listEnvironnement = data;
    });
  }

  onChangeEnvironnement(environnement, isChecked: boolean) {
    if (isChecked) {
      this.listSelectedUidEnvironnement.push(environnement);
    } else {
      const index = this.listSelectedUidEnvironnement.indexOf(environnement);
      this.listSelectedUidEnvironnement.splice(index, 1);
    }
  }

  isSelectedEnvironnement(environnement) {
    return this.listSelectedUidEnvironnement.indexOf(environnement) >= 0;
  }


  getAllStudyLevels() {
    this.firestoreService.getAllStudyLevels().subscribe((data) => {
      this.listStudyLevel = data;
      console.log('this.listStudyLevel', this.listStudyLevel);
    });
  }

  onChangeStudyLevels(studyLevels, isChecked: boolean) {
    if (isChecked) {
      this.listSelectedUidstudyLevel.push(studyLevels);
    } else {
      const index = this.listSelectedUidstudyLevel.indexOf(studyLevels);
      this.listSelectedUidstudyLevel.splice(index, 1);
    }
  }

  isSelectedStudyLevels(studyLevels) {
    return this.listSelectedUidstudyLevel.indexOf(studyLevels) >= 0;
  }

  submitUser(userForm) {
    const payload: any = {
      firstName: userForm.value.firstName,
      lastName: userForm.value.lastName,
      phoneNumber: userForm.value.phoneNumber,
      category: (userForm.value.uidCategory) ?
        this.listCategory.find(x => x.uid === userForm.value.uidCategory) : {},
      taxesTPSTVQ: userForm.value.taxesTPSTVQ,
      yearsExperience: userForm.value.yearsExperience,
      listSoftware: this.getListSoftwareWithUid(),
      listEnvironnement: this.getlistEnvironnementWithUid(),
      listStudyLevel: this.getListstudyLevelWithUid(),
      descriptionProfile: userForm.value.descriptionProfile,
      authorizedSmsEmail: userForm.value.authorizedSmsEmail,
      validDocuments: userForm.value.validDocuments,
    };
    if (this.hiddenOrderNumber) {
      payload.RAMQ = userForm.value.RAMQ;
      payload.professionalOrderNumber = userForm.value.professionalOrderNumber;
    }



    console.log(payload);
    this.addCurriculumVitae();
    this.addCIN();
    this.addLicense();
    this.userService.updateEmployee(payload)
      .then((data) => {
        console.log("data", data);
        this.toastrService.success('les infos sont bien enregistré');
      });

  }




  getListSoftwareWithUid() {
    const listSoftwareSelected: any[] = [];
    for (const uidSoftware of this.listSelectedUidSoftware) {
      listSoftwareSelected.push(this.listSoftware.find((x) => x.uid === uidSoftware));
    }
    return listSoftwareSelected;
  }

  getlistEnvironnementWithUid() {
    const listEnvironnementSlected: any[] = [];
    for (const uidEnvironnement of this.listSelectedUidEnvironnement) {
      listEnvironnementSlected.push(this.listEnvironnement.find((x) => x.uid === uidEnvironnement));
    }
    return listEnvironnementSlected;
  }

  getListstudyLevelWithUid() {
    const listLevelStudySelected: any[] = [];
    for (const uidStudyLevel of this.listSelectedUidstudyLevel) {
      listLevelStudySelected.push(this.listStudyLevel.find((x) => x.uid === uidStudyLevel));
    }
    return listLevelStudySelected;
  }
  getAllUsersExperience() {
    this.firestoreService.getAllExperience().subscribe((data: any) => {
      this.listeYearsExprence = data.sort((a, b) => (
        a.title > b.title ? 1 : -1));
    });
  }

  addCurriculumVitae() {
    if (this.selectedFileCv) {
      let basePath = '/CVS';
      const file = this.selectedFileCv.item(0);
      this.selectedFileCv = undefined;
      let currentFileUpload = new FileUpload(file);
      this.uploadFileServiceService.pushFileToStorage(currentFileUpload, this.uidCurrentUser, basePath);
    }
  }

  addCIN() {
    if (this.selectedFileCin) {
      let basePath = '/CIN';
      const file = this.selectedFileCin.item(0);
      this.selectedFileCin = undefined;
      let currentFileUpload = new FileUpload(file);
      this.uploadFileServiceService.pushFileToStorage(currentFileUpload, this.uidCurrentUser, basePath);
    }
  }

  addLicense() {
    if (this.selectedFileLicense) {
      let basePath = '/licenses';
      const file = this.selectedFileLicense.item(0);
      this.selectedFileLicense = undefined;
      let currentFileUpload = new FileUpload(file);
      this.uploadFileServiceService.pushFileToStorage(currentFileUpload, this.uidCurrentUser, basePath);
    }
  }

  selectFileCIN(event) {
    this.selectedFileCin = event.target.files;
  }

  selectFileCv(event) {
    this.selectedFileCv = event.target.files;
  }

  selectFileLicense(event) {
    this.selectedFileLicense = event.target.files;
  }


  DeleteCurriculumVitae() {
    let basePath = '/CVS';
    if (confirm('Êtes-vous sûr de supprimer votre cv ?') === true) {
      this.uploadFileServiceService.deleteFileUpload(this.user.cvFileName, basePath, this.uidCurrentUser);
      this.ngOnInit();
    }
  }

  DeleteCin() {
    let basePath = '/CIN';
    if (confirm('Êtes-vous sûr de supprimer votre CIN?') === true) {
      this.uploadFileServiceService.deleteFileUpload(this.user.cinFileName, basePath, this.uidCurrentUser);
      this.ngOnInit();
    }
  }

  DeleteLicense() {
    ;
    let basePath = '/licenses';
    if (confirm('Êtes-vous sûr de supprimer votre license ?') === true) {
      this.uploadFileServiceService.deleteFileUpload(this.user.licenseFileName, basePath, this.uidCurrentUser);
      this.ngOnInit();
    }
  }

}

