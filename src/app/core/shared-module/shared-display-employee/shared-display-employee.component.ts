import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-shared-display-employee',
  templateUrl: './shared-display-employee.component.html',
  styleUrls: ['./shared-display-employee.component.scss']
})
export class SharedDisplayEmployeeComponent implements OnInit {
  @Input() user: any;
  constructor() { }

  ngOnInit() {
  }

}
