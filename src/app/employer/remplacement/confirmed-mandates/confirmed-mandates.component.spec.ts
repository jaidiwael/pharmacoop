import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmedMandatesComponent } from './confirmed-mandates.component';

describe('ConfirmedMandatesComponent', () => {
  let component: ConfirmedMandatesComponent;
  let fixture: ComponentFixture<ConfirmedMandatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmedMandatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmedMandatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
