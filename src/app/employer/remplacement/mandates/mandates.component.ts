import { Component, OnInit,Input} from '@angular/core';

@Component({
  selector: 'app-mandates',
  templateUrl: './mandates.component.html',
  styleUrls: ['./mandates.component.scss']
})
export class MandatesComponent implements OnInit {
  public payload : object =  {
    statusMandate : 'free' , 
    owner : 'employer',
    method:'getListMandateByEmployer',
    parentLink:'employer/mandates',
    titlePage:'Mes mandats',
  }
  constructor() {}
  ngOnInit() {}
}
