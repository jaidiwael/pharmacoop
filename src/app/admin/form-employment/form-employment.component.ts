import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { FirestoreService } from '../../services/firestore.service';
import { Observable, Subject, of } from 'rxjs';
@Component({
  selector: 'app-form-employment',
  templateUrl: './form-employment.component.html',
  styleUrls: ['./form-employment.component.scss']
})
export class FormEmploymentComponent implements OnInit {
  @ViewChild(DataTableDirective) 
  public datatableElement: DataTableDirective;
  private dtTrigger = new Subject<any>();
  private listFormEmployment: any[];
  private loading = true;
  private FormEmploymentForm: FormGroup;
  private titleModal = "Ajouter une nouvelle form d'Employment";
  private titleButton = "Ajouter";
  public  dtOptions: DataTables.Settings = {};

  constructor(private formBuilder: FormBuilder,
              private firestoreService: FirestoreService) {
              this.createForm();
  }

  createForm() {
    this.FormEmploymentForm = this.formBuilder.group({
      title: ['', Validators.required],
      uid: ['']
    });
  }

  ngOnInit() {
    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getAllFormEmployment();
  }


  getAllFormEmployment() {
        this.listFormEmployment = [];
        this.firestoreService.getAllFormEmployment().subscribe((data: any[]) => {
        this.listFormEmployment = data ;
        this.dtTrigger.next();
        this.loading = false;
    });

  }

  rerender(): void {
      this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }

  submitFormEmployment(FormEmploymentForm) {
    const payload = { title: FormEmploymentForm.value.title };
    if (FormEmploymentForm.value.uid) {
      this.firestoreService.updateFormEmployment(FormEmploymentForm.value.uid,payload);
      this.getAllFormEmployment();
      this.rerender();
    } else {
      this.firestoreService.addFormEmployment(payload);
      this.getAllFormEmployment();
      this.rerender();
    }
    this.resetForm();

  }

  onEdit(FormEmployment) {
    this.titleModal = 'Modifier une Form d"employment';
    this.titleButton = 'Modifier';
      this.FormEmploymentForm = this.formBuilder.group({
      title: [FormEmployment.title, Validators.required],
      uid: [FormEmployment.uid]
    });
  }

  onDelete(employmentForm) {
    console.log("employmentForm",employmentForm) ;
    console.log("employmentForm",employmentForm.uid) ;
    if (confirm('Êtes-vous sûr de supprimer cet from employment ?') === true) {
      this.firestoreService.removeFormEmployment(employmentForm.uid);
      this.getAllFormEmployment();
      this.rerender();
    }
  }
  
  resetForm() {
    this.FormEmploymentForm.reset();
    this.titleModal = 'Ajouter un nouvelle form d"emploie';
    this.titleButton = 'Ajouter';
  }
}
