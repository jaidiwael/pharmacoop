import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Mandates } from '../classes/mandates.class';

@Injectable({
  providedIn: 'root'
})
export class MandatesService {

  private mandatesCollection: any;

  constructor(private afs: AngularFirestore) {
    this.mandatesCollection = this.afs.collection('mandates');
  }

  addMandates(mandates: Mandates) {
    return this.mandatesCollection.add(mandates);
  }

  removeMandates(uid: String) {
    return this.mandatesCollection.doc(uid).delete();
  }

  updateMandates(uid: String, mandates: Mandates) {
    return this.mandatesCollection.doc(uid).update(mandates);
  }

  getAllMandates(status: string) {
    return this.afs.collection('mandates', ref => ref.where("MandateStatus", '==', status))
      .snapshotChanges().pipe(map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const uid = a.payload.doc.id;
        return { uid, ...data };
      }))
      );
  }


  getAllMandatesByCategory(uidCategory: String, status: string) {
    return this.afs.collection('mandates',
      ref => ref.where('category.uid', '==', uidCategory)
        .where("publish", "==", true)
        .where("MandateStatus", "==", status))
      .snapshotChanges().pipe(map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const uid = a.payload.doc.id;
        return { uid, ...data };
      }))
      );
  }

  getMandatesByUid(uidMandates: string) {
    return this.afs.collection('mandates').doc(uidMandates).valueChanges();

  }

  getAllMandatesByEmployer(uidUser: String, status: String) {
    console.log("uid", uidUser, "status", status);
    return this.afs.collection('mandates',
      ref => ref.where('uidEmployer', '==', uidUser).where("MandateStatus", '==', status))
      .snapshotChanges().pipe(map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const uid = a.payload.doc.id;
        return { uid, ...data };
      }))
      );
  }

  getAllMandatesConfirmed(uidUser: String) {
    console.log("uidUser", uidUser);
    return this.afs.collection('mandates',
      ref => ref.where('uidSelectedEmployee', '==', uidUser))
      .snapshotChanges().pipe(map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data();
        const uid = a.payload.doc.id;
        return { uid, ...data };
      }))
      );
  }

}
