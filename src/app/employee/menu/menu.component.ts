import { Component, OnInit } from '@angular/core';
import { NavbarService } from "../../services/navbar.service";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  constructor(public navbarService: NavbarService) { this.navbarService.hide() }

  ngOnInit() {
  }
  logout() {
    this.navbarService.logout('/employee/login');
  }


}
