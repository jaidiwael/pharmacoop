import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  constructor(private _location: Location,
    private router: Router) { }

  ngOnInit() {
  }

  returnBack() {
    this._location.back();
  }

  goEmployerPage() {
    this.router.navigate(['/employer/login']);
  }

  goEmployeePage() {
    this.router.navigate(['/employee/login']);
  }
}
