import {Software} from "../classes/software.class"
export class Store {
    uid?: string;
    uidEnvironnement: string;
    environnement?: any;
    name: string;
    adress: string;
    city: string;
    province: string;
    postalCode: string;
    phone: string;
    description: string;
    listSelectedSoftware: Software[];

}
