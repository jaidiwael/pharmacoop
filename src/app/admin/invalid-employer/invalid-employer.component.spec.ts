import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvalidEmployerComponent } from './invalid-employer.component';

describe('InvalidEmployerComponent', () => {
  let component: InvalidEmployerComponent;
  let fixture: ComponentFixture<InvalidEmployerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvalidEmployerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvalidEmployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
