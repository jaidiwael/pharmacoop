import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AdminRoutingModule } from './admin.routing';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { CategoriesComponent } from './categories/categories.component';
import { SoftwareComponent } from './software/software.component';
import { PraticalEnvironnementComponent } from './pratical-environnement/pratical-environnement.component';
import { StudyLevelsComponent } from './study-levels/study-levels.component';

import { DataTablesModule } from 'angular-datatables';
import { LoadingModule } from 'ngx-loading';
import { MenuComponent } from './menu/menu.component';
import { EmployerComponent } from './employer/employer.component';
import { EmployeeComponent } from './employee/employee.component';
import { ExperienceComponent } from './experience/experience.component';
import { FormEmploymentComponent } from './form-employment/form-employment.component';

import { MandatesComponent } from './remplacement/mandates/mandates.component';
import { ConfirmedMandatesComponent } from './remplacement/confirmed-mandates/confirmed-mandates.component';
import { CandidatesMandatesComponent } from './remplacement/candidates-mandates/candidates-mandates.component';
import { AdsJobComponent } from './job/ads-job/ads-job.component';
import { CandidatesJobComponent } from './job/candidates-job/candidates-job.component';
import { ClosedMandatesComponent } from './remplacement/closed-mandates/closed-mandates.component';
import { InvalidEmployerComponent } from './invalid-employer/invalid-employer.component';
import { InvalidEmployeeComponent } from './invalid-employee/invalid-employee.component';

import { SharedModule } from "../core/shared-module/shared.module";

@NgModule({
    declarations: [
        LoginComponent,
        HomeComponent,
        UsersComponent,
        CategoriesComponent,
        SoftwareComponent,
        PraticalEnvironnementComponent,
        StudyLevelsComponent,
        MenuComponent,
        EmployerComponent,
        EmployeeComponent,
        ExperienceComponent,
        FormEmploymentComponent,
        MandatesComponent,
        ConfirmedMandatesComponent,
        AdsJobComponent,
        CandidatesJobComponent,
        CandidatesMandatesComponent,
        ClosedMandatesComponent,
        InvalidEmployerComponent,
        InvalidEmployeeComponent,
    ],
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        LoadingModule,
        ReactiveFormsModule,
        HttpClientModule,
        AdminRoutingModule,
        DataTablesModule,
        SharedModule
    ],
})
export class AdminModule { }
