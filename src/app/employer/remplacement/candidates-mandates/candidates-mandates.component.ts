import { Component, OnInit, ViewChild } from '@angular/core';
import { MandatesService } from "../../../services/mandates.service"
import { Mandates } from "../../../classes/mandates.class";
import { User } from "../../../classes/user.class";
import { UserService } from '../../../services/user.service';
import { Observable, Subject, of } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-candidates-mandates',
  templateUrl: './candidates-mandates.component.html',
  styleUrls: ['./candidates-mandates.component.scss']
})
export class CandidatesMandatesComponent implements OnInit {
  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  private dtTrigger = new Subject<any>();
  public dtOptions: DataTables.Settings = {};

  private loading: boolean = true;

  private titleModal: String = '';
  private titleButton: String = '';

  private employeeInfo: any;
  private listMandates: Mandates[] = [];
  private listEmployeeParticipate: User[] = [];
  private uidCurrentUser: string;
  private uidMandate: string;
  private hiddenButtonConfirmed: boolean = false;

  constructor(
    public route: ActivatedRoute,
    public mandatesService: MandatesService,
    public userService: UserService) {
    this.uidMandate = this.route.snapshot.params['uid'];
  }

  ngOnInit() {

    this.dtOptions = {
      info: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
      lengthChange: false,
      responsive: true,
      searching: true,
      retrieve: true,
    };
    this.getListEmployeeParticipateByMandate();
  }

  getListEmployeeParticipateByMandate() {
    this.mandatesService.getMandatesByUid(this.uidMandate).subscribe((mandate: Mandates) => {

      this.hiddenButtonConfirmed = (mandate.uidSelectedEmployee !== '') ? true : false;

      let i = 0;
      if (mandate.listUidEmployeeParticipate.length) {
        for (let uidEmployee of mandate.listUidEmployeeParticipate) {
          this.userService.getEmployeeInfoByUid(uidEmployee).subscribe((employee: any) => {
            this.listEmployeeParticipate.push(employee);
            i++;
            console.log((i === mandate.listUidEmployeeParticipate.length));
            if (i === mandate.listUidEmployeeParticipate.length) this.dtTrigger.next();
          })
        }
      }
      this.loading = false;
    })

  }


  rerender(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }

  onDisplay(candidate: any) {
    this.titleModal = 'Informations candidate';
    this.employeeInfo = candidate;
  }

  applyEmployeeForThisMandate(employee) {
    if (confirm('Voulez-vous choisis ce candidat pour ce mandat ?') === true) {
      const payload = {
        MandateStatus: "confirmed",
        uidSelectedEmployee: employee.uid,
      }
      this.mandatesService.updateMandates(this.uidMandate, payload);
    }

  }

}





































































