import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-applied-job',
  templateUrl: './applied-job.component.html',
  styleUrls: ['./applied-job.component.scss']
})
export class AppliedJobComponent implements OnInit {

  public adsInfo: any = {
    owner : 'employee',
    getAllAds: false,
    parentLink:'employee/search-job',
    titlePage:'Emplois appliqués',
  }
  constructor() { }
  ngOnInit() { }

}
