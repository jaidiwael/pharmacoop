import { Component, OnInit } from '@angular/core';
import {NavbarService} from "../../services/navbar.service" ;
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(public router : Router ,public navbarService :NavbarService) { this.navbarService.hide()}

  ngOnInit() {
  }
  logout(){
    this.navbarService.logout('/employer/login'); 
  }


}
